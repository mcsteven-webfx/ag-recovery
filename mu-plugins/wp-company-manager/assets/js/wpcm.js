var WPCM = ( function( WPCM, $ ) {

    // Window Completely Loaded
    $( window ).on( 'load', function() {

        // Initialize models, collections, and views
        WPCM.Locations = new WPCM.Collections.Locations();

        if ( $( '#closest_location' ).length ) {
            WPCM.User = new WPCM.Models.User();
            WPCM.ClosestLocation = new WPCM.Views.ClosestLocation( { el: ( '#closest_location' ), model: WPCM.User } );
        }

        if ( $( '#wpcm_zip' ).length ) {
            WPCM.LocationSearch  = new WPCM.Views.SearchByZip( { el: $( '#wpcm_zip' ), collection: WPCM.Locations } ) ;
        }

        if ( $( '#locations_map' ).length ) {

            // Initialize Map
            WPCM.Map = new WPCM.Views.Map( { el: $( '#locations_map' ), collection: WPCM.Locations } );

            if ( $( '#locations_list' ).length ) {
                WPCM.List = new WPCM.Views.List( { el: $( '#locations_list' ), collection: WPCM.Locations } );
            }

            if ( typeof WPCM_LOCATION_ID !== 'undefined' ) {

                // Single location map
                WPCM.Locations.fetch( {
                    data: {
                        location_id: WPCM_LOCATION_ID
                    }
                });
            } else {

                if ( $('#state').val() ) {
                    var state = $('#state').val();

                    WPCM.Locations.fetch( {
                        data: {
                            state: state
                        }
                    });
                } else {
                    WPCM.Locations.fetch( {
                        reset: true,
                        success: function( collection, response, options ) {
                            WPCM.Locations.originalModels = collection.toJSON();
                            WPCM.Locations.originalCollection = new Backbone.Collection( WPCM.Locations.originalModels );
                        }
                    } );
                }

            }
        }
    });

    /**
     * FEDS - No Need to Edit This Section
     */
    WPCM.Models      = WPCM.Models      || {};
    WPCM.Collections = WPCM.Collections || {};
    WPCM.Views       = WPCM.Views       || {};
    WPCM             = _.extend( WPCM, Backbone.Events );

    WPCM.Models.Location = Backbone.Model.extend( {

        urlRoot: '/wp-json/wpcm-locations/vl/view',

        default: {
            page: '',
            title: '',
            address: '',
            phone: '',
            directions: '',
            url: '',
            hours: '',
        }
    });

    WPCM.Models.User = Backbone.Model.extend( {

        default: {
            geo: {
                latitude: 0,
                longitude: 0
            },
            location: ''
        },

        initialize: function() {
            this.on( 'change:geo', this._setClosestLocation, this );
            $( document ).on( 'click', '#get_geolocation', $.proxy( this.getLocation, this ) );
            this.maybeSetLocation();
        },

        maybeSetLocation: function() {
            // Checks PFX and cookie for geo info to set closest location
            if( _.isEmpty( this.get( 'geo' ) ) ) {
                if( typeof PersonalizeFX !== 'undefined' && typeof PersonalizeFX.uLoc.lat !== 'undefined' ) {
                    this.set( 'geo', {
                        latitude: PersonalizeFX.uLoc.lat,
                        longitude: PersonalizeFX.uLoc.lng
                    });
                } else {
                    if ( typeof Cookies.get( 'wpcm-geo' ) !== 'undefined' ) {
                        var pos = Cookies.getJSON( 'wpcm-geo' );
                        this._setLocation({
                            coords: {
                                latitude: pos.latitude,
                                longitude: pos.longitude
                            }
                        });
                    }
                }
            }
            if( ! _.isEmpty( this.get( 'geo' ) ) ) {
                this._setClosestLocation();
            }
        },

        getLocation: function(e) {
            e.preventDefault();
            this.askForLocation();
        },

        askForLocation: function() {
            if ( navigator.geolocation ) {
                navigator.geolocation.getCurrentPosition( _.bind( this._setLocation, this ), _.bind( this._locationError ) );
            } else {
                console.log( 'Geolocation is not supported in this browser.' );
                WPCM.trigger( 'User/Location/Error' );
            }
        },

        _setLocation: function( pos ) {
            if ( typeof Cookies.get( 'wpcm-geo' ) == 'undefined' ) {
                Cookies.set(
                    'wpcm-geo',
                    { latitude: pos.coords.latitude, longitude: pos.coords.longitude },
                    { expires: 30 }
                );
            }
            this.set( 'geo', {
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude
            });
        },

        _locationError: function( err ) {
            console.log( err );
            console.warn( 'ERROR(' + err.code + '): ' + err.message );
            WPCM.trigger( 'User/Location/Error' );
        },

        _setClosestLocation: function() {
            var self = this;
            // Fetch closest location, set cookie
            if ( typeof Cookies.get( 'wpcm-closest' ) == 'undefined' ) {
                $.get(
                    '/wp-json/wpcm-locations/v1/closest',
                    {
                        latitude: this.get( 'geo' ).latitude,
                        longitude: this.get( 'geo' ).longitude
                    }
                ).done( function( response ) {
                    self.set( 'location', response );
                    Cookies.set( 'wpcm-closest', response, { expires: 30 } );
                });
            } else {
                this.set( 'location', Cookies.getJSON( 'wpcm-closest' ) );
            }
        }
    });

    WPCM.Collections.Locations = Backbone.Collection.extend( {

        url: '/wp-json/wpcm-locations/v1/view',
        model: WPCM.Models.Location,

        initialize: function() {
            this.originalModels = [];
            this.originalCollection = [];
        },

		filter: function( filterVal ) {
			var filtered = this.originalCollection.filter( function( location ) {
				return ( _.contains( location.get( 'filteredAttribute' ), filterVal ) );
			});
			this.reset( filtered );
		},

		clearFilter: function() {
			this.reset( this.originalModels );
		}
    });

    /**
     * Renders the Marker Info Window Html
     */
    WPCM.Views.Marker = Backbone.View.extend( {

        template: null,

        initialize: function() {
            this.template = _.template( $( '#markerTemplate' ).html() );
        },

        render: function() {
            this.$el.html( this.template( this.model.toJSON() ) );
            return this;
        }
    });

    /**
     * Renders out the Google Map
     */
    WPCM.Views.Map = Backbone.View.extend( {

        map: null,
        infoWindow: null,
        filterView: null,
        markers: [],

        defaults: {
            zoom: 9,
            scrollwheel: false,
            // MAP STYLING: replace [] with the JS array from Snazzy Maps
            styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
        },

        initialize: function( options ) {
            this.setElement( options.el );
            this.map        = new google.maps.Map( document.getElementById( 'locations_map' ), this.defaults );
            this.infoWindow = new google.maps.InfoWindow( { content: '' } );
            this.icon       =  {
                url: WPCM.plugin_url + 'assets/img/pin.png?v3',
            };
            // Add Listeners to the Locations Collection
            this.listenTo( this.collection, 'reset', this.render );
            this.listenTo( this.collection, 'sync', this.render );
        },

        addMarker: function( location ) {
            var attrs = location.toJSON(),
                self  = this;

            if ( attrs.lat == '' || attrs.lng == '' ) {
                //empty lat/lng from say a no-address location from the client, breaks the bounds
                console.log( attrs );
                return;
            }

            var iconMarker = this.icon;

            if( attrs.category == 'middlesex-recovery' ) {
                iconMarker = WPCM.plugin_url + 'assets/img/pin2.png?v3';
            }

            var position = new google.maps.LatLng( attrs.lat, attrs.lng ),
                marker   = new google.maps.Marker( {
                    animation: google.maps.Animation.DROP,
                    position : position,
                    title    : attrs.post.post_title,
                    icon     : iconMarker
                });
            marker.setMap( this.map );
            this.markers.push( marker );
            this.bounds.extend( marker.position );
            google.maps.event.addListener( marker, 'click', function() {
                self.infoWindow.setContent( new WPCM.Views.Marker( { model: location } ).render().el );
                self.infoWindow.open( self.map, this );
            });
        },

        render: function( collection ) {
            this.removeMarkers();
            this.bounds = new google.maps.LatLngBounds();
            if( ! collection.isEmpty() ){
                collection.each( this.addMarker, this );
                this.map.setCenter( this.bounds.getCenter() );
                this.map.fitBounds( this.bounds );
                // Make sure we don't get overly zoomed in
                var zoom = this.map.getZoom();
                this.map.setZoom( zoom > 16 ? 16 : zoom );
                if( ! _.isEmpty( WPCM.action.display ) ) {
                    var marker = _.findWhere( this.markers, { title: WPCM.action.display } );
                    if( typeof marker !== 'undefined' ) {
                        google.maps.event.trigger( marker, 'click' );
                        this.map.setCenter( marker.getPosition() );
                    }
                }
                // If there's only one marker, automatically open infobox
                if( 1 === collection.length ) {
                    google.maps.event.trigger( this.markers[0], 'click' );
                }
            }
            return this;
        },

        addModelToMap: function() {
            var attrs = this.model.toJSON();
            this.addMarker( this.model );
            this.map.setCenter( new google.maps.LatLng( attrs.lat, attrs.lng ) );
        },

        removeMarkers: function() {
            var self = this;
            for ( var i = 0; i < this.markers.length; i++ ) {
                self.markers[ i ].setMap( null );
            }
            this.markers = [];
        }
    });

    /**
     * Renders out the List of locations
     */
    WPCM.Views.List = Backbone.View.extend( {

        template: null,

        initialize: function( options ) {
            this.setElement( options.el );
            this.listenTo( this.collection, 'sync', this.render );
            this.listenTo( this.collection, 'reset', this.render );
        },

        render: function( collection ) {
            this.$el.empty();
            if( ! this.collection.isEmpty() ){
                this.collection.each( this.addOne, this );
            } else {
                this.$el.append( '<p class="col-sm-12 no-result">Sorry, we don\'t have any locations within your current search distance.</p>' );
            }
            return this;
        },

        addOne: function( model ) {
            var item = new WPCM.Views.ListItem( { model: model } );
            this.$el.append( item.render().el );
            return this;
        }
    });

    /**
     * Renders each individual location in list
     */
    WPCM.Views.ListItem = Backbone.View.extend( {

        initialize: function( options ) {
            this.template = _.template( $( '#listItemTemplate' ).html() );
        },

        render: function() {
            this.setElement( this.template( this.model.toJSON() ) );
            return this;
        }
    });

    /**
     * Renders out closest location widget
     */
    WPCM.Views.ClosestLocation = Backbone.View.extend( {

        template: null,

        initialize: function( options ) {
            this.setElement( options.el );
            this.template = _.template( $( '#closestLocationTemplate' ).html() );
            this.model.on( 'change:location', this.render, this );
            if( ! _.isEmpty( this.model.get( 'location' ) ) ) {
                this.render();
            }
        },

        render: function( location ) {
            if( ! _.isEmpty( this.model.get( 'location' ) ) ){
                this.$el.html( this.template( this.model.get( 'location' ) ) );
            }
            return this;
        }
    });

    /**
     * Renders out closest location from zipcode
     */
    WPCM.Views.SearchByZip = Backbone.View.extend( {

        initialize: function( options ) {
            this.setElement( options.el );
            if( ! _.isEmpty( WPCM.action.zipcode ) ) {
                this.$el.find( '.wpcm-zip__form' ).trigger( 'submit' );
            }
        },

        events: {
            'submit .wpcm-zip__form': 'onSubmit'
        },

        onSubmit: function( event ) {
            event.preventDefault();
            var zipcode  = this.$el.find( '.wpcm-zip__zipcode' ).val(),
                distance = this.$el.find( '.wpcm-zip__distance' ).val(),
                imahuman = this.$el.find( '#imahuman' ).val(),
                state = this.$el.find( '#state' ).val();

            this.collection.fetch( {
                reset: true,
                data: {
                    zipcode: zipcode,
                    distance: distance,
                    imahuman: imahuman,
                    state: state
                }
            });
        }
    });

    return WPCM;

})( WPCM || {}, jQuery );
