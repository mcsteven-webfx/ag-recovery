<?php

if ( ! defined('ABSPATH') ) exit;

class CM_Post_Types {

    protected static $_instance;

    public static function instance() {
        if ( ! isset( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function __construct() {
        add_action( 'init', array( $this, 'register_post_types' ) );
    }

    public static function register_post_types() {

        if ( ! function_exists( 'get_field' ) || ! $location_slug = get_field( 'location_slug', 'option' ) ) {
            $location_slug = 'company/locations';
        }

        // register location post type
        register_post_type(
            'location',
            array(
                'labels' => array(
                    'name' => 'Locations',
                    'singular_name' => 'Location',
                    'add_new' => 'New Location',
                    'add_new_item' => 'Add New Location',
                    'edit_item' => 'Edit Location',
                    'new_item' => 'New Location',
                    'all_items' => 'Locations',
                    'view_item' => 'View Location',
                    'search_items' => 'Search Locations',
                    'not_found' =>  'No locations found',
                    'not_found_in_trash' => 'No locations found in trash',
                    'parent_item_colon' => '',
                    'menu_name' => 'Company'
                ),
                'public' => true,
                'has_archive' => false,
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'show_in_menu' => true,
                'supports' => array( 'title', 'editor' ),
                'rewrite' => array(
                   'slug' => $location_slug,
                   'with_front' => false
                ),
                'menu_icon' => 'dashicons-location-alt'
            )
        );


        $labels = array(
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        );
        
        register_taxonomy(
            'location_category',
            'location',
            array(
                'labels'            => $labels,
                'rewrite'           => array(
                    'slug'       => 'location/category',
                    'with_front' => false,
                ),
                'hierarchical'      => true,
                'show_admin_column' => true,
            )
        );
    }
}

CM_Post_Types::instance();
