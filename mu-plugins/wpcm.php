<?php

/**
 * Plugin Name: Company Manager
 * Plugin URI: https://www.webfx.com
 * Description: Manage company locations.
 * Version: 3.2
 * Author: WebFX
 * Author URI: https://www.webfx.com
 *
 * Text Domain: wpcm
 */

$plugin_index = __DIR__ . '/wp-company-manager/wp-company-manager.php';
if( is_file( $plugin_index ) ) {
	require_once( $plugin_index );
}