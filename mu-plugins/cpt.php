<?php

/*
 *
 * Plugin Name: FX Custom Post Types and Taxonomies
 * Description: Adding of Custom Post Types and Taxonomies
 *
*/

/*
 *
 * Adding of CPTs
 *
 */
 add_action( 'init', 'cptui_register_my_cpts' );
 function cptui_register_my_cpts() {

 	/**
 	 * Post Type: Faqs.
 	 */

 	$labels = [
 		"name" => __( "Faqs", "fx" ),
 		"singular_name" => __( "Faq", "fx" ),
 		"menu_name" => __( "Faqs", "fx" ),
 	];

 	$args = [
 		"label" => __( "Faqs", "fx" ),
 		"labels" => $labels,
 		"description" => "",
 		"public" => true,
 		"publicly_queryable" => true,
 		"show_ui" => true,
 		"show_in_rest" => true,
 		"rest_base" => "",
 		"rest_controller_class" => "WP_REST_Posts_Controller",
 		"has_archive" => "faqs",
 		"show_in_menu" => true,
 		"show_in_nav_menus" => true,
 		"delete_with_user" => false,
 		"exclude_from_search" => false,
 		"capability_type" => "post",
 		"map_meta_cap" => true,
 		"hierarchical" => false,
 		"rewrite" => [ "slug" => "faqs", "with_front" => true ],
 		"query_var" => true,
 		"menu_position" => 4,
 		"menu_icon" => "dashicons-editor-help",
 		"supports" => [ "title", "editor", "thumbnail", "excerpt", "revisions" ],
 		"show_in_graphql" => false,
 	];

 	register_post_type( "faqs", $args );

 	/**
 	 * Post Type: Team Members.
 	 */

 	$labels = [
 		"name" => __( "Team Members", "fx" ),
 		"singular_name" => __( "Team Member", "fx" ),
 		"menu_name" => __( "Team Members", "fx" ),
 	];

 	$args = [
 		"label" => __( "Team Members", "fx" ),
 		"labels" => $labels,
 		"description" => "",
 		"public" => true,
 		"publicly_queryable" => true,
 		"show_ui" => true,
 		"show_in_rest" => true,
 		"rest_base" => "",
 		"rest_controller_class" => "WP_REST_Posts_Controller",
 		"has_archive" => "members",
 		"show_in_menu" => true,
 		"show_in_nav_menus" => true,
 		"delete_with_user" => false,
 		"exclude_from_search" => false,
 		"capability_type" => "post",
 		"map_meta_cap" => true,
 		"hierarchical" => false,
 		"rewrite" => [ "slug" => "teams", "with_front" => true ],
 		"query_var" => true,
 		"menu_position" => 4,
 		"menu_icon" => "dashicons-image-filter",
 		"supports" => [ "title", "editor", "thumbnail", "custom-fields", "revisions" ],
 		"show_in_graphql" => false,
 	];

 	register_post_type( "teams", $args );

    /**
     * Post Type: Guides and Forms.
     */

    $labels = [
        "name" => __( "Guides and Forms", "fx" ),
        "singular_name" => __( "Guide and Form", "fx" ),
    ];

    $args = [
        "label" => __( "Guides and Forms", "fx" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => [ "slug" => "form-guides", "with_front" => true ],
        "query_var" => true,
        "menu_position" => 4,
        "menu_icon" => "dashicons-format-aside",
        "supports" => [ "title", "thumbnail", "custom-fields", "revisions" ],
        "show_in_graphql" => false,
    ];

    register_post_type( "form-guides", $args );
 }
 /*
 *
 * Adding of Custom Taxonomies
 *
 */
 add_action( 'init', 'cptui_register_my_taxes' );
 function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Position Categories.
	 */

	$labels = [
		"name" => __( "Position Categories", "fx" ),
		"singular_name" => __( "Position Category", "fx" ),
	];


	$args = [
		"label" => __( "Position Categories", "fx" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'position', 'with_front' => true,  'hierarchical' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "position",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "position", [ "teams" ], $args );

	/**
	 * Taxonomy: PDF Categories.
	 */

	$labels = [
		"name" => __( "PDF Categories", "fx" ),
		"singular_name" => __( "PDF Category", "fx" ),
	];


	$args = [
		"label" => __( "PDF Categories", "fx" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'pdf', 'with_front' => true,  'hierarchical' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "pdf",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "pdf", [ "form-guides" ], $args );

    /**
     * Taxonomy: FAQS Categories.
     */

    $labels = [
        "name" => __( "FAQs Categories", "fx" ),
        "singular_name" => __( "FAQs Category", "fx" ),
    ];


    $args = [
        "label" => __( "FAQs Categories", "fx" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => [ 'slug' => 'faqs-cat', 'with_front' => true,  'hierarchical' => true, ],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "faqs-cat",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
        "show_in_graphql" => false,
    ];
    register_taxonomy( "faqs-cat", [ "faqs" ], $args );
}
