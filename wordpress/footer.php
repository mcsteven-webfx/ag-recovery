        <!-- Footer -->
        <footer class="footer">
            <div class="container footer-wrapper">
                <div class="footer-top">
                    <?php
                        $logo_id    = fx_get_client_logo_image_id();
                        $home_url   = get_home_url();
                        $template_url = get_template_directory_uri();
                    ?>
                    <!-- Logo -->
                    <a class="footer__logo" href="<?php echo $home_url; ?>">
                        <?php echo fx_get_image_tag( $logo_id, 'logo' ); ?>
                    </a>
                    <!-- Navigation -->
                    <?php
                        // Output the Main navigation
                        wp_nav_menu(
                            [
                                'container'         => 'ul',
                                'menu_class'        => 'footer-nav',
                                'depth'             => 1,
                                'theme_location'    => 'footer_menu',
                            ]
                        );
                    ?>

                    <!-- Social -->
                    <div class="footer-social">
                        <?php
                            $facebook = get_field('facebook', 'option');
                            $instagram = get_field('instagram', 'option');
                            $linkedin = get_field('linkedin', 'option');
                        ?>
                        <div class="footer-social-wrapper">
                            <a class="footer-social__link" href="<?php echo $facebook; ?>" target="_blank">
                                <span class="icon-facebook"></span>
                            </a>
                            <a class="footer-social__link" href="<?php echo $instagram; ?>" target="_blank">
                                <span class="icon-instagram"></span>
                            </a>
                            <a class="footer-social__link" href="<?php echo $linkedin; ?>" target="_blank">
                                <span class="icon-linkedin"></span>
                            </a>
                        </div>
                        <a class="footer-go-top hidden-xs-down" href="#">
                            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="">
                        </a>
                    </div>
                </div>

                <div class="footer-bottom">
                    <a class="footer-go-top hidden-sm" href="#">
                        <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="">
                    </a>

                    <div class="footer-other">
                        <?php
                            $sitemap = get_field('sitemap', 'option');
                            $sitemap_url = $sitemap['url'];
                            $sitemap_title = $sitemap['title'];
                            $sitemap_target = $sitemap['target'] ? $sitemap['target'] : '_self';

                            $privacy = get_field('privacy_policy', 'option');
                            $privacy_url = $privacy['url'];
                            $privacy_title = $privacy['title'];
                            $privacy_target = $privacy['target'] ? $privacy['target'] : '_self';
                        ?>
                        <a class="footer-other__links" href="<?php echo $sitemap_url; ?>" target="<?php echo $sitemap_target; ?>"><?php echo $sitemap_title; ?></a>
                        <a class="footer-other__links" href="<?php echo $privacy_url; ?>" target="<?php echo $privacy_target; ?>"><?php echo $privacy_title; ?></a>
                        <p class="footer-other__copyright">Copyright © 2021. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
