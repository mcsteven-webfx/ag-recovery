<?php
/*
 *
 * Template Name: Location Template
 *
 * Location template for Location Page Homepage
 *
 */
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<main class="location">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</main>

<?php get_footer();
