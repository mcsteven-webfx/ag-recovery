<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Add Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&family=Poppins:wght@400;500;600;700;800&family=Shadows+Into+Light&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php wp_body_open(); ?>

    <?php
        // gets client logo image set in Theme Settings
        $logo_id    = fx_get_client_logo_image_id();
        $home_url   = get_home_url();
        $template_url = get_template_directory_uri();

        // Contact Button Link
        $link = get_field( 'link', 'option' );
        $link_url = $link['url'];
        $link_title = $link['title'];
        $link_target = $link['target'] ? $link['target'] : '_self';
    ?>

    <!-- Header -->
    <header class="header js-sticky">
        <div class="header-wrapper">
            <!-- Header Logo -->
            <a class="header-logo" href="<?php echo $home_url; ?>">
                <?php echo fx_get_image_tag( $logo_id, 'header-logo__img' ); ?>
            </a>
            <!-- Header Mobile -->
            <div class="header-mobile">
                <!-- Open Header Search -->
                <button class="header-search js-search">
                    <img class="header-search__icon" src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
                </button>
                <!-- Open Header Hamburger -->
                <button class="header-hamburger js-mobile">
                    <span class="header-hamburger__line bg-color--green"></span>
                    <span class="header-hamburger__line margin-btm--zero bg-color--green"></span>
                    <span class="header-hamburger__text text-color--green">MENU</span>
                </button>
            </div>
            <!-- Header Desktop -->
            <div class="header-desktop">
                <!-- Header Desktop Top -->
                <ul class="header-desktop-top">
                    <li class="header-desktop-top-item">
                        <?php
                            $phone      = fx_get_client_phone_number();
                            $phone_link = fx_get_client_phone_number( true );
                        ?>
                        <a class="header-desktop__number text-color--green" href="tel:<?php echo $phone_link ?>"><?php echo $phone; ?></a>
                    </li>
                    <li class="header-desktop-top-item">
                        <a class="header-desktop__search js-search">
                            <img src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
                        </a>
                    </li>
                </ul>
                <!-- Header Desktop Down -->
                <div class="header-desktop-down">
                    <?php
                        // Output the Main navigation
                        wp_nav_menu(
                            [
                                'container'         => 'ul',
                                'depth'             => 3,
                                'theme_location'    => 'main_menu',
                            ]
                        );
                    ?>
                    <a class="btn btn-tertiary header-desktop__contact" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a>
                </div>

            </div>
            <!-- Header Mobile Dropdown -->
            <div class="header-dropdown js-mobile-dropdown">
                <?php
                    // Output the Main navigation
                    wp_nav_menu(
                        [
                            'container'         => 'ul',
                            'depth'             => 3,
                            'theme_location'    => 'main_menu',
                        ]
                    );
                ?>

                <?php
                    $link = get_field( 'link', 'option' );
                    $link_url = $link['url'];
                ?>
                <a class="header-dropdown__button btn btn-tertiary" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a>
            </div>
        </div>
    </header>
    <!-- Header Sticky -->
    <header class="header-sticky">
        <!-- Header Sticky Wrapper -->
        <div class="header-sticky-wrapper">
            <!-- Header Logo -->
            <a class="header-logo" href="<?php echo $home_url; ?>">
                <?php echo fx_get_image_tag( $logo_id, 'header-logo__img' ); ?>
            </a>
            <div class="header-desktop-down">
                <?php
                    // Output the Main navigation
                    wp_nav_menu(
                        [
                            'container'         => 'ul',
                            'depth'             => 3,
                            'theme_location'    => 'main_menu',
                        ]
                    );

                    $phone      = fx_get_client_phone_number();
                    $phone_link = fx_get_client_phone_number( true );
                ?>
                <a class="btn btn-tertiary header-desktop__contact" href="tel:<?php echo $phone_link; ?>"><?php echo $phone; ?></a>
            </div>
        </div>
    </header>
    <!-- Search Form -->
    <?php get_search_form(); ?>
