<?php
/*
 *
 * Text With Background Block
 *
 */
?>
<?php $template_url = get_template_directory_uri(); ?>
<section class="textbg">
    <div class="textbg-wrapper">
        <!-- Background -->
        <?php
        $image = get_field('image');
        $size = 'full';

        if( $image ):
            echo wp_get_attachment_image( $image, $size, '', ['class' => 'textbg__background'] );
        endif;
        ?>
        <!-- Editor -->
        <div class="container">
            <div class="textbg-content">
                <div class="wysiwyg wysiwyg-component textbg-editor container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
                    <?php echo get_field('content'); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Outline -->
    <?php $outline = get_field('outline_background'); ?>

    <img class="textbg__outline visible-xs visible-xxs" src="<?php echo $template_url; ?>/assets/img/textbg-outline<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
    <img class="textbg__outline visible-sm" src="<?php echo $template_url; ?>/assets/img/textbg-outline-sm<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
    <img class="textbg__outline visible-md visible-lg" src="<?php echo $template_url; ?>/assets/img/textbg-outline-md<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
</section>
