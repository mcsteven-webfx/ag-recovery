<?php
/*
 *
 * Gainboro Container Block
 *
 */
?>
<?php
    $background = get_field('container');
    $spacing = get_field('remove_top_spacing');
    $classes = '';

     if ( strcmp($spacing,'remove') == 0 ):
         $classes .= 'background-remove--top-spacing ';
     elseif( strcmp($spacing,'negative') == 0 ):
         $classes .= 'background-negative--top-spacing ';
     endif;

     if ( strcmp($background,'white') == 0 ):
         $classes .= 'background--default ';
     endif;
?>
<section class="background <?php echo $classes; ?>">
    <InnerBlocks />
</section>
