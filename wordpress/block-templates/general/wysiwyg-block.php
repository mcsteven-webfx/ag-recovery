<?php
/*
 *
 * WYSIWYG Block
 *
 */
?>
<div class="wysiwyg wysiwyg-block container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
	<?php echo get_field('content'); ?>
</div>
