<?php
/*
 *
 * Two Image Block
 *
 */
?>
<?php
    $large_image = get_field('large_image');
    $small_image = get_field('small_image');
    $size = 'full';
?>
<section class="twoimage<?php echo ( empty($small) ) ? ' oneimage' : '';?> container">
    <div class="row">
        <!-- Left 2 Image -->
        <div class="col-xxs-12 col-md-6 hidden-sm-down wow hide--wow animate__animated animate__fadeInLeft" data-wow-offset="250">
            <div class="twoimage-figure">
                <?php

                $large_classes = '';
                $large_classes .= 'twoimage__img ';

                if( strcmp(get_field('large_image_left_or_right'), 'right') == 0 ):
                    $large_classes .= 'flip';
                endif;

                $small_classes = '';
                $small_classes .= 'twoimage__img--absolute ';

                if( strcmp(get_field('small_image_left_or_right'), 'right') == 0 ):
                    $small_classes .= 'flip';
                endif;

                if( $large_image ):
                    echo wp_get_attachment_image( $large_image, $size, '', ['class' => $large_classes] );
                endif;

                if( !empty($small_image) ):
                    echo wp_get_attachment_image( $small_image, $size, '', ['class' => $small_classes] );
                endif;
                ?>
            </div>
        </div>
        <!-- Editor -->
        <div class="col-xxs-12 col-md-6 wow hide--wow animate__animated animate__fadeInRight" data-wow-offset="250">
            <!-- Editor -->
            <div class="wysiwyg wysiwyg-component">
                <?php echo get_field('content'); ?>
            </div>
        </div>
    </div>
</section>
