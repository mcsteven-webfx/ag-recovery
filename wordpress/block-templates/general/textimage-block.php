<?php
/*
 *
 * Half text / Half "small" image
 *
 */
?>
<div class="textimage container">
    <div class="row">
        <!-- Image -->
        <div class="col-xxs-12 col-sm-5 col-md-4 wow hide--wow animate__animated animate__fadeInLeft textimage-image" data-wow-offset="250">
            <?php
            $image = get_field('image');
            $size = 'full';
            $classes = '';
            $classes .= 'textimage__image ';

            if( strcmp(get_field('image_left_or_right'), 'right') == 0 ):
                $classes .= 'flip';
            endif;

            if( $image ):
                echo wp_get_attachment_image( $image, $size, '', ['class' => $classes] );
            endif; ?>
        </div>
        <!-- Text Editor -->
        <div class="col-xxs-12 col-sm-7 col-md-8 wow hide--wow animate__animated animate__fadeInRight textimage-content" data-wow-offset="250">
            <div class="wysiwyg wysiwyg-component">
                <?php echo get_field('content'); ?>
            </div>
        </div>

    </div>
</div>
