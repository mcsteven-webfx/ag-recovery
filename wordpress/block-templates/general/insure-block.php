<?php
/*
 *
 * Insurance Company Blocks
 *
 */
?>
<?php $template_url = get_template_directory_uri(); ?>
<div class="insure">
    <div class="insure-wrapper container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
        <!-- Title -->
        <h4><?php echo get_field('heading'); ?></h4>
        <!-- Slider -->
        <div class="insure-slider fx-slider">
            <?php if( have_rows('logos') ): ?>
                <?php while( have_rows('logos') ): the_row() ?>
                    <?php
                        $logo = get_sub_field('logo');
                        $size = 'full';
                    ?>
                    <div class="insure-slide fx-slide">
                        <?php
                        if( $logo ):
                            echo wp_get_attachment_image( $logo, '', '', '' );
                        endif;
                        ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <!-- Slider Arrow Nav -->
        <div class="insure-nav">
            <!-- Previous -->
            <button class="insure__button button--prev js-prev">
                <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Left">
            </button>
            <!-- Next -->
            <button class="insure__button button--next js-next">
                <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
            </button>
        </div>
    </div>
</div>
