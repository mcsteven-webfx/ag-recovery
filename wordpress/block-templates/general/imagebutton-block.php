<?php
/*
 *
 * Text with Blurb Image Button
 *
 */
?>
<?php $template_url = get_template_directory_uri(); ?>
<section class="imagebutton">
    <div class="imagebutton-wrapper">
        <div class="container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
            <!-- Image & Button content -->
            <div class="imagebutton-content">
                <img class="imagebutton-content__image" src="<?php echo $template_url; ?>/assets/icons/logo-with-bg.svg" alt="Logo">
                <!-- Editor -->
                <div class="wysiwyg wysiwyg-component">
                    <?php echo get_field('content'); ?>
                </div>
            </div>
            <!-- Image & Button blurb -->
            <div class="imagebutton-blurb">
                <?php if( have_rows('image_button_blurb') ): ?>
                    <?php while( have_rows('image_button_blurb') ): the_row(); ?>
                        <?php
                            $link = get_sub_field('link');
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';

                            $icon = get_sub_field('icon');
                            $size = 'full';

                            $icon_hover = get_sub_field('icon_on_hover');
                        ?>
                        <a class="imagebutton-item" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                            <?php
                            if( $icon ):
                                echo wp_get_attachment_image( $icon, $size, '', ['class' => 'imagebutton__icon'] );
                                echo wp_get_attachment_image( $icon, $size, '', ['class' => 'imagebutton__hover'] );
                            endif;
                            ?>
                            <span class="imagebutton__title"><?php echo $link_title; ?></span>
                        </a>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <?php
                $button = get_field('button_link');
                $button_url = $button['url'];
                $button_title = $button['title'];
                $button_target = $button['target'] ? $button['target'] : '_self';
            ?>

            <a class="btn-filled text-left" href="<?php echo $button_url; ?>" target="<?php echo $button_target; ?>">
                <?php echo $button_title; ?>
                <span class="btn-arrow btn-arrow--transparent">
                    <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                </span>
            </a>
        </div>
    </div>

    <?php $outline = get_field('outline_background'); ?>
    <img class="imagebutton__outline visible-xs visible-xxs" src="<?php echo $template_url; ?>/assets/img/imagebutton-outline<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
    <img class="imagebutton__outline visible-sm" src="<?php echo $template_url; ?>/assets/img/imagebutton-outline-sm<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
    <img class="imagebutton__outline visible-md visible-lg" src="<?php echo $template_url; ?>/assets/img/imagebutton-outline-md<?php echo ( strcmp($outline, 'gainsboro') == 0 ) ? '-bg' : ''?>.svg" alt="">
</section>
