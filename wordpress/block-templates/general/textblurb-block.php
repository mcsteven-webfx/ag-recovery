<?php
/*
 *
 * Text with Blurb Block
 *
 */
?>

<div class="textblurb wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
    <div class="textblurb-wrapper container">
        <!-- Editor -->
        <div class="wysiwyg wysiwyg-component">
            <?php echo get_field('content'); ?>
        </div>
        <!-- Blurb -->
        <div class="textblurb-blurb">
            <?php if( have_rows('blurb') ): ?>
                <?php while( have_rows('blurb') ): the_row(); ?>
                    <div class="textblurb-box">
                        <div class="textblurb-figure">
                            <?php
                            $icon = get_sub_field('icon');
                            $size = 'full';

                            if( $icon ):
                                echo wp_get_attachment_image( $icon, $size, '', ['class' => 'textblurb__icon'] );
                            endif;
                            ?>
                        </div>

                        <span class="textblurb__title"><?php echo get_sub_field('title'); ?></span>
                        <p class="textblurb__text"><?php echo get_sub_field('summary'); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
