<?php

/**
 * $template note:
 *
 * Block names should be prefixed with acf/. So if the name you specified in
 * fx_register_block is 'your-block-name', the name you should use here is
 * 'acf/your-block-name'
 */

$template = [
	['acf/homepage-slider'],
	['acf/homepage-about'],
	['acf/homepage-treatment'],
	['acf/homepage-steps'],
	['acf/homepage-location'],
	['acf/homepage-insurance'],
	['acf/homepage-resource'],
];

?>

<div>
    <InnerBlocks template="<?php echo esc_attr( wp_json_encode( $template ) )?>" templateLock="all" />
</div>
