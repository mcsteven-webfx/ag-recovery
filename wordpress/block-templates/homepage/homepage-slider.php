<?php
/*
 *
 * Homepage Slider Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<section class="home-slider">
    <!-- Home Slide -->
    <div class="home-slide fx-slider">
        <!-- Individual Items -->
        <?php if( have_rows('slide') ): ?>
            <?php while( have_rows('slide') ): the_row(); ?>
                <div class="home-slide-item fx-slide">
                    <!-- Figure -->
                    <div class="home-slide-figure">
                        <!-- Image -->
                        <?php
                        $image = get_sub_field('background_image');
                        $image_mobile = get_sub_field('background_image_mobile');
                        $size = 'full';

                        $classes = '';
                        $classes .= 'home-slide__img visible-md visible-lg ';

                        $classes_mobile = '';
                        $classes_mobile .= 'home-slide__img hidden-md-up ';

                        if( strcmp(get_sub_field('background_image_left_or_right'), 'right') == 0 ):
                            $classes .= 'flip';
                        endif;

                        if( strcmp(get_sub_field('background_image_left_or_right_mobile'), 'right') == 0 ):
                            $classes_mobile .= 'flip';
                        endif;

                        if( $image ):
                            echo wp_get_attachment_image( $image, $size, '', ['class' => $classes] );
                            echo wp_get_attachment_image( $image_mobile, $size, '', ['class' => $classes_mobile] );
                        endif;
                        ?>
                        <!-- Image Background -->
                        <div class="home-slide__bg"></div>
                    </div>
                    <!-- Text Wrapper -->
                    <div class="home-slide-content wow hide--wow animate__animated animate__fadeInLeft">
                        <?php the_sub_field('content'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <!-- Home Slide Outline Bottom -->
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-slide-outline.svg', 'home-slide-outlined hidden-sm-up' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-slide-outline-md.svg', 'home-slide-outlined visible-sm' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-slide-outline-sm.svg', 'home-slide-outlined visible-md visible-lg' ); ?>
</section>
