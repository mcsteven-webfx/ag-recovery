<?php
/*
 *
 * Homepage Steps Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="home-step container">
    <div class="row">
        <div class="col-xxs-12 col-md-4 wow hide--wow animate__animated animate__fadeInLeft" data-wow-offset="250">
            <!-- Editor -->
            <div class="wysiwyg wysiwyg-component textbg-wrapper container">
                <?php the_field('editor'); ?>
            </div>
        </div>
        <div class="col-xxs-12 col-md-8 wow hide--wow animate__animated animate__fadeInRight" data-wow-offset="250">
            <!-- Step List -->
            <ul class="home-step-list">
                <?php if( have_rows('step') ): ?>
                    <?php while( have_rows('step') ): the_row(); ?>
                        <?php
                            //Number
                            $current = get_row_index();
                            $number = str_pad($current, 2, "0", STR_PAD_LEFT);

                            //Link
                            $link = get_sub_field('link');
                            $link_url = $link['url'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <!-- Step List Item -->
                        <a class="home-step-item" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                            <span class="home-step__number text-color--white bg-color--green"><?php echo $number; ?></span>
                            <h4 class="home-step__title text-color--blue"><?php echo get_sub_field('title'); ?></h4>
                            <p class="home-step__paragraph"> <?php echo get_sub_field('summary_text'); ?> </p>
                            <button class="home-step__link bg-color--blue">
                                <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                            </button>
                        </a>
                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>
        </div>

        <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-step-bg.svg', 'home-step__background' ); ?>
    </div>
</div>
