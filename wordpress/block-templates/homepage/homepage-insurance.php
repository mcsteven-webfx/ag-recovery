<?php
/*
 *
 * Homepage Insurance Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="home-cover container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="100">
    <!-- Title -->
    <h4><?php echo get_field('heading'); ?></h4>
    <!-- Slider -->
    <div class="home-cover-slider fx-slider">
        <?php if( have_rows('logos') ): ?>
            <?php while( have_rows('logos') ): the_row(); ?>
                <?php
                    $logo = get_sub_field('logo');
                    $size = 'full';
                ?>
                <div class="home-cover-slide fx-slide">
                    <?php
                    if( $logo ):
                        echo wp_get_attachment_image( $logo, '', '', '' );
                    endif;
                    ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <!-- Slider Arrow Nav -->
    <div class="home-cover-nav">
        <!-- Previous -->
        <button class="home-cover__button button--prev js-prev">
            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Left">
        </button>
        <!-- Next -->
        <button class="home-cover__button button--next js-next">
            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
        </button>
    </div>
</div>
