<?php
/*
 *
 * Homepage Location Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="home-location">
    <!-- Outline -->
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-location-outline.svg', 'home-location-outlined hidden-sm-up' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-location-outline-sm.svg', 'home-location-outlined visible-sm' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-location-outline-lg.svg', 'home-location-outlined visible-md visible-lg' ); ?>

    <div class="home-location-wrapper container text-center">
        <!-- Logo -->
        <img class="home-location__logo wow hide--wow animate__animated animate__fadeInUp" src="<?php echo $template_url; ?>/assets/icons/logo-with-bg.svg" alt="Logo" data-wow-offset="100">
        <!-- Editor -->
        <div class="wysiwyg wysiwyg-component textbg-wrapper container text-center wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="100">
            <?php the_field('editor'); ?>
        </div>
        <!-- Form -->
        <div class="home-map-wrapper">
            <?php
                $state = get_field('location_select_state');
                echo do_shortcode("[locations-map state={$state}]");
            ?>
        </div>
    </div>
</div>
