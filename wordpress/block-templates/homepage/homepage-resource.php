<?php
/*
 *
 * Homepage Resource Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="home-resource">
    <div class="container wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
        <!-- Wrapper -->
        <div class="home-resource-wrapper">
            <!-- Heading -->
            <div class="home-resource-heading">
                <img class="home-resource__logo" src="<?php echo $template_url; ?>/assets/icons/logo-no-bg.svg" alt="Logo">
                <h2>Our Resources</h2>
            </div>
            <!-- Buttons -->
            <div class="home-resource-buttons">
                <button class="home-resource-button js-resource active" data-tab="blog">BLOG</button>
                <button class="home-resource-button js-resource" data-tab="faqs">FAQS</button>
            </div>
        </div>
        <!-- List -->
        <div class="row home-resource-blurb js-resource-tab active" data-target="blog">
            <?php
            $order = get_field('order');
            $order_by = get_field('order_by');
            $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => $order,
                'orderby' => $order_by,
            );

            $post = new WP_Query( $args );

            $counter = 1;
            ?>

            <?php while ( $post->have_posts() ): $post->the_post(); ?>
                <?php
                $value = get_field('post_featured', get_the_ID());

                if( $value == true ):
                ?>
                    <!-- Item -->
                    <div class="col-xxs-12 col-sm-6 col-md-4 <?php echo ( $counter == 2 ) ? 'hidden-xs-down ' : ''; ?> <?php echo ( $counter == 3 ) ? 'hidden-sm-down ' : ''; ?> animate__animated animate__fadeIn">
                        <!-- Box -->
                        <a class="home-resource-box" href="<?php the_permalink(); ?>">
                            <!-- Category -->
                            <div class="home-resource-category">
                                <img class="home-resource__icon btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/book-open.svg" alt="Blog">
                                <span class="home-resource__cat text-color--white">BLOG</span>
                            </div>

                            <?php
                                $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                $image_alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
                            ?>
                            <!-- Image -->
                            <img class="home-resource__img" src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                            <!-- Content -->
                            <div class="home-resource-content">
                                <h3><?php echo get_the_title(); ?></h3>
                                <p><?php echo get_the_excerpt(); ?></p>
                                <span class="home-resource-readmore text-color--green">
                                    READ MORE
                                    <span class="arrow-key--right"></span>
                                </span>
                            </div>
                        </a>
                    </div>
                    <?php $counter++; ?>
                <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>

        <!-- List -->
        <div class="row home-resource-blurb js-resource-tab" data-target="faqs">
            <?php
            $order = get_field('order');
            $order_by = get_field('order_by');
            $args = array(
                'post_type' => 'faqs',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => $order,
                'orderby' => $order_by,
            );

            $post = new WP_Query( $args );

            $counter = 1;
            ?>

            <?php while ( $post->have_posts() ): $post->the_post(); ?>
                <?php
                $value = get_field('featured_resources', get_the_ID());

                if( $value == true ):
                ?>
                    <!-- Item -->
                    <div class="col-xxs-12 col-sm-6 col-md-4 <?php echo ( $counter == 2 ) ? 'hidden-xs-down ' : ''; ?> <?php echo ( $counter == 3 ) ? 'hidden-sm-down ' : ''; ?> animate__animated animate__fadeIn">
                        <!-- Box -->
                        <a class="home-resource-box" href="<?php the_permalink(); ?>">
                            <!-- Category -->
                            <div class="home-resource-category">
                                <img class="home-resource__icon btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/question-icon.svg" alt="Blog">
                                <span class="home-resource__cat text-color--white">FAQS</span>
                            </div>

                            <?php
                                $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                $image_alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
                            ?>
                            <!-- Image -->
                            <img class="home-resource__img" src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                            <!-- Content -->
                            <div class="home-resource-content">
                                <h3><?php echo get_the_title(); ?></h3>
                                <p><?php echo get_the_excerpt(); ?></p>
                                <span class="home-resource-readmore text-color--green">
                                    READ MORE
                                    <span class="arrow-key--right"></span>
                                </span>
                            </div>
                        </a>
                    </div>
                    <?php $counter++; ?>
                <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>

        <!-- Blog Button -->
        <div class="home-resource-link text-center js-resource-link active" data-target="blog">
            <a href="<?php echo get_home_url(); ?>/resources/blog/" class="btn-outlined btn-outlined-tertiary">
                View All Blog Posts
                <span class="btn-arrow">
                    <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                </span>
            </a>
        </div>

        <!-- Blog Button -->
        <div class="home-resource-link text-center js-resource-link" data-target="faqs">
            <a href="<?php echo get_home_url(); ?>/resources/faqs/" class="btn-outlined btn-outlined-tertiary">
                View All FAQs Posts
                <span class="btn-arrow">
                    <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                </span>
            </a>
        </div>

    </div>
</div>
