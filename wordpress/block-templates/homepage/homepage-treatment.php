<?php
/*
 *
 * Homepage Treatment Block Template
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<section class="home-program">
    <!-- Outlined -->
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-programs-outline.svg', 'home-program-outlined hidden-sm-up' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-programs-outline-sm.svg', 'home-program-outlined visible-sm' ); ?>
    <?php echo fx_get_image_tag( 'https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/09/home-programs-outline-lg.svg', 'home-program-outlined visible-md visible-lg' ); ?>
    <!-- Program Wrapper -->
    <div class="home-program-wrapper bg-color--blue">
        <div class="container">
            <!-- Logo -->
            <img class="home-program__logo wow hide--wow animate__animated animate__fadeInUp" src="<?php echo $template_url; ?>/assets/icons/logo-with-bg.svg" alt="Logo" data-wow-offset="100">
            <!-- Editor -->
            <div class="wysiwyg wysiwyg-component textbg-wrapper container wow hide--wow animate__animated animate__fadeInUp text-center" data-wow-offset="100">
                <?php the_field('editor'); ?>
            </div>
            <!-- Blurb -->
            <div class="home-program__blurb row wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="100">
                <?php if( have_rows('programs') ): ?>
                    <?php while( have_rows('programs') ): the_row(); ?>
                        <?php
                            $icon = get_sub_field('icon');
                            $icon_hover = get_sub_field('icon_hover');
                            $size = 'full';

                            //Link
                            $link = get_sub_field('icon_link');
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a class="home-program-blurb-item" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                            <?php
                            if( $icon ):
                                echo wp_get_attachment_image( $icon, $size, '', ['class' => 'home-program-blurb__icon'] );
                            endif;
                            if( !empty($icon_hover) ):
                                echo wp_get_attachment_image( $icon_hover, $size, '', ['class' => 'home-program-blurb__hover'] );
                            endif;
                            ?>
                            <h5 class="home-program-blurb__title"><?php echo $link_title; ?></h5>
                        </a>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!-- Button -->
            <?php
                //Link
                $link = get_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="home-program__link btn-filled wow hide--wow animate__animated animate__fadeInUp" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>" data-wow-offset="100">
                <?php echo $link_title; ?>
                <span class="btn-arrow btn-arrow--transparent">
                    <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                </span>
            </a>
        </div>
    </div>

</section>
