<?php
/*
 *
 * Innerpage Form and Guides
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<section class="guides-container">
    <!-- Outline -->
    <img class="guides__outline" src="<?php echo $template_url; ?>/assets/img/inner-outline.svg" alt="">
    <!-- Guides Wrapper -->
    <div class="guides-wrapper">
        <div class="container">
            <?php $paged = get_query_var('paged') ? get_query_var('paged') : 1; ?>
            <div class="guides-box wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250" data-page="<?php echo $paged; ?>">
                <!-- Navigation Buttons -->
                <div class="guides-nav">
                    <p class="guides-nav__title">Filter By:</p>
                    <div class="guides-btn-wrapper">
                        <button class="guides__btn js-filter" data-filter="guides">
                            <span class="guides__btn-circle active"></span>
                            <p class="guides__btn-title">Guides</p>
                        </button>
                        <button class="guides__btn js-filter" data-filter="forms">
                            <span class="guides__btn-circle"></span>
                            <p class="guides__btn-title">Forms</p>
                        </button>
                        <button class="guides__btn js-filter" data-filter="all">
                            <span class="guides__btn-circle"></span>
                            <p class="guides__btn-title">Show All</p>
                        </button>
                    </div>
                </div>
                <!-- Search Bar -->
                <div class="search-bar">
                    <form class="search-bar-wrapper">
                        <img class="search-bar__icon" src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
                        <input class="search-bar__search" type="text" name="search" placeholder="Search Documents">
                        <button type="submit" class="search-bar__link js-search bg-color--blue" href="./">
                            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                        </button>
                    </form>
                    <label class="search__error">Search field is empty!</label>
                </div>
                <!-- Guide Ajax Container -->
                <div class="guides-ajax"></div>

                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
