<?php
/*
 *
 * Innerpage Contact
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="contact-wrapper">
    <div class="container">
        <form class="contact-form">
            <!-- Contact Form Group -->
            <div class="contact-form-group">
                <!-- First Name -->
                <div class="contact-form-input">
                    <h4>First Name*</h4>
                    <input class="danger" type="text" placeholder="Jane">
                    <span class="danger">Please complete this field</span>
                </div>
                <!-- Last Name -->
                <div class="contact-form-input">
                    <h4>Last Name*</h4>
                    <input type="text" placeholder="Doe">
                </div>
            </div>
            <!-- Contact Form Group -->
            <div class="contact-form-group">
                <!-- Email -->
                <div class="contact-form-input">
                    <h4>Email Address*</h4>
                    <input type="email" placeholder="address@domain.com">
                </div>
                <!-- Phone Number -->
                <div class="contact-form-input">
                    <h4>Phone Number*</h4>
                    <input type="text" placeholder="(###) ###-####">
                </div>
            </div>
            <!-- Contact Form Group -->
            <div class="contact-form-group">
                <!-- Email -->
                <div class="contact-form-input full">
                    <h4>Message*</h4>
                    <textarea name="name" placeholder="How can we help you?"></textarea>
                </div>
            </div>
            <!-- Contact Form Group -->
            <div class="contact-form-group">
                <p>This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service  apply.</p>
            </div>
            <!-- Contact Form Group -->
            <div class="contact-form-group center last">
                <a class="btn-filled btn-filled--whitebg" href="">
                    Send Message
                    <span class="btn-arrow btn-arrow--transparent">
                        <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                    </span>
                </a>
            </div>
            <!-- Contact Bg -->
            <img class="contact__bg" src="<?php echo $template_url; ?>/assets/img/contact-bg.svg" alt="">
        </form>
    </div>
    <img class="contact__outline" src="<?php echo $template_url; ?>/assets/img/contact-outline.svg" alt="">
</div>
<!-- Add Contact Loading Spinner Here -->
<div class="contact-load">

</div>
