<?php
/*
 *
 * Innerpage State Block
 *
 */
?>

<?php
$template_url = get_template_directory_uri(); ?>
<section class="location-cards">
    <!-- Outline -->
    <img class="location-cards__outline" src="<?php echo $template_url; ?>/assets/img/inner-outline.svg" alt="">
    <!-- Wrapper -->
    <div class="location-cards-wrapper">
        <div class="container">
            <?php if( have_rows('state_links', 'option') ): ?>
                <?php while( have_rows('state_links', 'option') ): the_row(); ?>
                    <?php //Variables
                        $value = get_field('choose_state');
                        $link = get_sub_field('state_page');
                        if( $link ):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        endif;

                        $state_value = strtolower($link_title);
                    ?>

                    <?php if ( strcmp ($value, $state_value) == 0 || strcmp ($value, 'default') == 0 ): ?>
                        <div class="location-state wow animate__animated animate__fadeInUp" data-wow-offset="250">

                            <a class="location-state__title"><h2><?php echo $link_title; ?></h2></a>

                            <div class="location-state-list">
                                <?php
                                //Query
                                $args = array(
                                    'post_type' => 'location',
                                    'post_status' => 'publish',
                                    'posts_per_page' => -1,
                                    'order' => 'ASC',
                                );

                                $post = new WP_Query( $args );

                                if( $post->have_posts() ):
                                    while( $post->have_posts() ): $post->the_post();
                                        $state = get_field('state', get_the_ID());
                                        $address = get_field('address', get_the_ID());
                                        $hours = get_field('hours', get_the_ID());
                                        $page = get_field('page', get_the_ID());
                                        if( strcmp($state, $link_title) == 0 ):
                                ?>
                                            <div class="location-state-box">
                                                <h2 class="location-state__city"><?php echo get_the_title(); ?></h2>
                                                <h4 class="location-state__state"><?php echo $link_title; ?></h4>
                                                <p class="location-state__address"><?php echo $address; ?></p>
                                                <p class="location-state__address"><?php echo $hours; ?></p>
                                                <?php
                                                $link = get_field('phone', get_the_ID());
                                                ?>
                                                <a class="location-state__contact" href="tel:<?php echo $link; ?>">
                                                    <img src="<?php echo $template_url; ?>/assets/icons/location-phone.svg" alt="Location Phone">
                                                    <?php echo $link; ?>
                                                </a>
                                                <a class="btn btn-quaternary location-state__link" href="<?php echo $page; ?>">More Info</a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>

                                <?php wp_reset_postdata(); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
