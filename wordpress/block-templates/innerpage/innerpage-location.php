<?php
/*
 *
 * Innerpage Location Block
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<div class="container">
    <section class="location-wrapper wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
        <?php 
            $state = get_field('location_select_state');
            echo do_shortcode("[locations-map state={$state}]");
        ?>
    </section>
</div>
