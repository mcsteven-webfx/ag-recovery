<?php
/**
* Echos whatever was place in the WYSIWYG editor
* and wraps it in the form structure. The
* innerpage/inner-jotform-contact.css file
* is used to do general styling and all form
* styling is done inside of JotForms.
*/
$template_url = get_template_directory_uri();
$iframe_code  = get_field('jotform_iframe_code');
?>
<div class='contact-wrapper'>
    <div class='container'>
        <div class="contact-form">
            <?php echo $iframe_code; ?>
            <img class="contact__bg" src="<?php echo $template_url; ?>/assets/img/contact-bg.svg" alt="">
        </div>
    </div>
    <img class="contact__outline" src="<?php echo $template_url; ?>/assets/img/contact-outline.svg" alt="">
</div>
