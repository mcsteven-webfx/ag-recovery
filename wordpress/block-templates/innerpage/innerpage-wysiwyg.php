<?php
/*
 *
 * Innerpage WYSIWYG
 *
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<section class="inner-content <?php echo get_field('full_width') == true ? 'full-width' : '';  ?> wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
    <div class="inner-content-figure">
        <img class="inner-content__logo" src="<?php echo $template_url; ?>/assets/icons/logo-no-bg.svg" alt="AppleGate Logo">
    </div>
    <div class="container">
        <div class="wysiwyg wysiwyg-component">
            <?php echo get_field('content'); ?>
        </div>
    </div>
</section>
