<?php
/*
 *
 * Innerpage Custom
 *
 */
?>
<div class="container">
    <section class="custom-blurb wow hide--wow animate__animated animate__fadeInUp" data-wow-offset="250">
        <?php
        $order = get_field('order');
        $order_by = get_field('order_by');

        $args = array(
            'post_type' => 'teams',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => $order,
            'orderby' => $order_by,
        );

        $post = new WP_Query( $args );

        while ( $post->have_posts() ) : $post->the_post();
        ?>
            <a class="custom-box" href="<?php echo get_permalink(); ?>">
                <?php
                    $terms = get_the_terms( $post->ID, 'position' );
                    $position = '';
                    foreach($terms as $term) {
                        $position = $term->name;
                    }
                    $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    $image_alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
                ?>
                <img src="<?php echo $image_url ?>" alt="<?php $image_alt; ?>">
                <h3><?php echo get_the_title(); ?></h3>

                <h4><?php echo $position ?></h4>
                <span class="btn btn-quaternary">Read More</span>
            </a>
        <?php endwhile; ?>
    </section>
</div>
