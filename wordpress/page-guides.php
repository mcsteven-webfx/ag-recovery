<?php
/*
 *
 * Template Name: Forms and Guides Template
 *
 * Custom template used for Forms and Guides Page
 *
 */
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<main class="guides">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
