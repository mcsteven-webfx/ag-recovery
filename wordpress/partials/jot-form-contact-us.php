<?php $template_url = get_template_directory_uri(); ?>
<script src="https://cdn01.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn02.jotfor.ms/static/jotform.forms.js?3.3.27725" type="text/javascript"></script>
<script type="text/javascript">
	JotForm.newDefaultTheme = true;
	JotForm.extendsNewTheme = false;
	JotForm.newPaymentUIForNewCreatedForms = false;
	JotForm.newPaymentUI = true;

	JotForm.init(function() {
		/*INIT-START*/
		if (window.JotForm && JotForm.accessible) $('input_1').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_2').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_3').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_4').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_8').setAttribute('tabindex', 0);
		/*INIT-END*/
	});

	JotForm.prepareCalculationsOnTheFly([null, {
		"name": "firstName",
		"qid": "1",
		"text": "First Name",
		"type": "control_textbox"
	}, {
		"name": "lastName",
		"qid": "2",
		"text": "Last Name",
		"type": "control_textbox"
	}, {
		"name": "email",
		"qid": "3",
		"text": "Email",
		"type": "control_textbox"
	}, {
		"name": "phone",
		"qid": "4",
		"text": "Phone",
		"type": "control_textbox"
	}, null, {
		"name": "selectTreatment6",
		"qid": "6",
		"text": "Select Treatment Center",
		"type": "control_dropdown"
	}, {
		"name": "bestTime",
		"qid": "7",
		"text": "Best time to call",
		"type": "control_dropdown"
	}, {
		"name": "message",
		"qid": "8",
		"text": "Message",
		"type": "control_textarea"
	}, null, {
		"name": "firstName10",
		"qid": "10",
		"text": "Send Message",
		"type": "control_button"
	}, {
		"description": "",
		"name": "pleaseVerify",
		"qid": "11",
		"text": "Please verify that you are human",
		"type": "control_captcha"
	}]);
	setTimeout(function() {
		JotForm.paymentExtrasOnTheFly([null, {
			"name": "firstName",
			"qid": "1",
			"text": "First Name",
			"type": "control_textbox"
		}, {
			"name": "lastName",
			"qid": "2",
			"text": "Last Name",
			"type": "control_textbox"
		}, {
			"name": "email",
			"qid": "3",
			"text": "Email",
			"type": "control_textbox"
		}, {
			"name": "phone",
			"qid": "4",
			"text": "Phone",
			"type": "control_textbox"
		}, null, {
			"name": "selectTreatment6",
			"qid": "6",
			"text": "Select Treatment Center",
			"type": "control_dropdown"
		}, {
			"name": "bestTime",
			"qid": "7",
			"text": "Best time to call",
			"type": "control_dropdown"
		}, {
			"name": "message",
			"qid": "8",
			"text": "Message",
			"type": "control_textarea"
		}, null, {
			"name": "firstName10",
			"qid": "10",
			"text": "Send Message",
			"type": "control_button"
		}, {
			"description": "",
			"name": "pleaseVerify",
			"qid": "11",
			"text": "Please verify that you are human",
			"type": "control_captcha"
		}]);
	}, 20);
</script>


<div class="contact-wrapper">
    <div class="container">
        <form class="contact-form" action="https://submit.jotform.com/submit/211465049618458/" method="post" name="form_211465049618458" id="211465049618458" accept-charset="utf-8" autocomplete="on">
        	<input type="hidden" name="formID" value="211465049618458" />
        	<input type="hidden" id="JWTContainer" value="" />
        	<input type="hidden" id="cardinalOrderNumber" value="" />
        	<div role="main">
                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- First Name -->
                    <div class="contact-form-input jf-required" id="cid_1">
                        <h4>First Name*</h4>
                        <input type="text" id="input_1" name="q1_firstName" data-type="input-textbox" class="danger validate[required]" data-defaultvalue="" value="" data-component="textbox" required="" placeholder="Jane">
                        <span class="danger">Please complete this field</span>
                    </div>
                    <!-- Last Name -->
                    <div class="contact-form-input jf-required" id="cid_2">
                        <h4>Last Name*</h4>
                        <input type="text" id="input_2" name="q2_lastName" data-type="input-textbox" class="validate[required]" data-defaultvalue="" value="" data-component="textbox" required="" placeholder="Doe">
                    </div>
                </div>

                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- Email -->
                    <div class="contact-form-input jf-required" id="cid_3">
                        <h4>Email Address*</h4>
                        <input type="email" id="input_3" name="q3_email" data-type="input-textbox" class="validate[required, Email]" data-defaultvalue="" value="" data-component="textbox" required="" placeholder="address@domain.com">
                    </div>
                    <!-- Phone Number -->
                    <div class="contact-form-input jf-required" id="cid_4">
                        <h4>Phone Number*</h4>
                        <input type="text" id="input_4" name="q4_phone" data-type="input-textbox" class="validate[required]" data-defaultvalue="" value="" data-component="textbox" required="" placeholder="(###) ###-####">
                    </div>
                </div>

                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- Treatment Center -->
                    <div class="jf-required contact-form-input" data-type="control_dropdown" id="cid_6">
                        <h4>Select Treatment Center*</h4>
    					<select class="validate[required]" id="input_6" name="q6_selectTreatment6">
    						<option value=""> Select Treatment Center * </option>
    						<option value="Baton Rouge"> Baton Rouge </option>
    						<option value="Bossier City"> Bossier City </option>
    						<option value="Bowling Green"> Bowling Green </option>
    						<option value="Camden"> Camden </option>
    						<option value="Charleston"> Charleston </option>
    						<option value="Crestview Hills"> Crestview Hills </option>
    						<option value="Dickson"> Dickson </option>
    						<option value="Dyersburg"> Dyersburg </option>
    						<option value="El Dorado"> El Dorado </option>
    						<option value="Elyria"> Elyria </option>
    						<option value="Huber Heights"> Huber Heights </option>
    						<option value="Jackson"> Jackson </option>
    						<option value="Lake Charles"> Lake Charles </option>
    						<option value="Lewisville"> Lewisville </option>
    						<option value="Mansfield"> Mansfield </option>
    						<option value="Martinsburg"> Martinsburg </option>
    						<option value="Metairie"> Metairie </option>
    						<option value="Middletown"> Middletown </option>
    						<option value="Monroe"> Monroe </option>
    						<option value="Monroeville"> Monroeville </option>
    						<option value="Mt. Carmel"> Mt. Carmel </option>
    						<option value="Nashville"> Nashville </option>
    						<option value="Oakdale"> Oakdale </option>
    						<option value="Plano"> Plano </option>
    						<option value="Port Matilda"> Port Matilda </option>
    						<option value="Richmond"> Richmond </option>
    						<option value="Savannah"> Savannah </option>
    						<option value="Selinsgrove"> Selinsgrove </option>
    						<option value="Selmer"> Selmer </option>
    						<option value="Slidell"> Slidell </option>
    						<option value="Thibodaux"> Thibodaux </option>
    						<option value="Washington - Locust Ave"> Washington - Locust Ave </option>
    						<option value="Washington - N. Main St."> Washington - N. Main St. </option>
    						<option value="Williamsport"> Williamsport </option>
    						<option value="Willoughby"> Willoughby </option>
    					</select>
        			</div>
                    <!-- Time to Call -->
                    <div class="contact-form-input" data-type="control_dropdown" id="cid_7">
                        <h4> Best time to call </h4>
                        <select class="" id="input_7" name="q7_bestTime">
                            <option value=""> Best time to call </option>
                            <option value="Morning (before 12 PM)"> Morning (before 12 PM) </option>
                            <option value="Afternoon (12 PM - 5 PM)"> Afternoon (12 PM - 5 PM) </option>
                            <option value="Night (after 5 PM)"> Night (after 5 PM) </option>
                        </select>
                    </div>
                </div>

                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- Your Message -->
                    <div class="contact-form-input full"  id="cid_8">
                        <h4>Message*</h4>
                        <textarea id="input_8" class="validate[required]" name="q8_message" data-component="textarea" required="" placeholder="How can we help you?"></textarea>
                    </div>
                </div>

                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- reCAPTCHA -->
                    <div class="contact-form-input jf-required" data-type="control_captcha" id="cid_11">
        				<h4>Please verify that you are human*</h4>
    					<section data-wrapper-react="true">
    						<div id="recaptcha_input_11" data-component="recaptcha" data-callback="recaptchaCallbackinput_11" data-expired-callback="recaptchaExpiredCallbackinput_11">
    						</div>
    						<input type="hidden" id="input_11" class="hidden validate[required]" name="recaptcha_visible" required="" />
    						<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=explicit&amp;onload=recaptchaLoadedinput_11"></script>
    						<script type="text/javascript">
    							var recaptchaLoadedinput_11 = function() {
    								window.grecaptcha.render($("recaptcha_input_11"), {
    									sitekey: '6LdU3CgUAAAAAB0nnFM3M3T0sy707slYYU51RroJ',
    								});
    								var grecaptchaBadge = document.querySelector('.grecaptcha-badge');
    								if (grecaptchaBadge) {
    									grecaptchaBadge.style.boxShadow = 'gray 0px 0px 2px';
    								}
    							};

    							/**
    							 * Called when the reCaptcha verifies the user is human
    							 * For invisible reCaptcha;
    							 *   Submit event is stopped after validations and recaptcha is executed.
    							 *   If a challenge is not displayed, this will be called right after grecaptcha.execute()
    							 *   If a challenge is displayed, this will be called when the challenge is solved successfully
    							 *   Submit is triggered to actually submit the form since it is stopped before.
    							 */
    							var recaptchaCallbackinput_11 = function() {
    								var isInvisibleReCaptcha = false;
    								var hiddenInput = $("input_11");
    								hiddenInput.setValue(1);
    								if (!isInvisibleReCaptcha) {
    									if (hiddenInput.validateInput) {
    										hiddenInput.validateInput();
    									}
    								} else {
    									triggerSubmit(hiddenInput.form)
    								}

    								function triggerSubmit(formElement) {
    									var button = formElement.ownerDocument.createElement('input');
    									button.style.display = 'none';
    									button.type = 'submit';
    									formElement.appendChild(button).click();
    									formElement.removeChild(button);
    								}
    							}

    							// not really required for invisible recaptcha
    							var recaptchaExpiredCallbackinput_11 = function() {
    								var hiddenInput = $("input_11");
    								hiddenInput.writeAttribute("value", false);
    								if (hiddenInput.validateInput) {
    									hiddenInput.validateInput();
    								}
    							}
    						</script>
    					</section>
        			</div>
                </div>

                <!-- Contact Form Group -->
                <div class="contact-form-group">
                    <!-- Submit -->
                    <div class="form-line" data-type="control_button" id="id_10">
        				<div id="cid_10" class="form-input-wide" data-layout="full">
        					<div data-align="center" class="form-buttons-wrapper form-buttons-center   jsTest-button-wrapperField">
        						<button id="input_10" type="submit" class="form-submit-button submit-button jf-form-buttons jsTest-submitField btn-filled btn-filled--whitebg" data-component="button" data-content="">
        							Send Message
                                    <span class="btn-arrow btn-arrow--transparent">
                                        <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                                    </span>
        						</button>
        					</div>
        				</div>
        			</div>
        			<div style="display:none">
        				Should be Empty:
        				<input type="text" name="website" value="" />
        			</div>
                </div>

        	</div>
        	<script>
        		JotForm.showJotFormPowered = "0";
        	</script>
        	<script>
        		JotForm.poweredByText = "Powered by JotForm";
        	</script>
        	<input type="hidden" class="simple_spc" id="simple_spc" name="simple_spc" value="211465049618458" />
        	<script type="text/javascript">
        		var all_spc = document.querySelectorAll("form[id='211465049618458'] .si" + "mple" + "_spc");
        		for (var i = 0; i < all_spc.length; i++) {
        			all_spc[i].value = "211465049618458-211465049618458";
        		}
        	</script>
            <!-- Contact Bg -->
            <img class="contact__bg" src="<?php echo $template_url; ?>/assets/img/contact-bg.svg" alt="">
        </form>

    </div>
    <img class="contact__outline" src="<?php echo $template_url; ?>/assets/img/contact-outline.svg" alt="">
</div>
<!-- Add Contact Loading Spinner Here -->
<div class="contact-load">

</div>
