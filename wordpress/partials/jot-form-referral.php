<script src="https://cdn01.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
<script src="https://cdn02.jotfor.ms/static/jotform.forms.js?3.3.27725" type="text/javascript"></script>
<script src="https://cdn03.jotfor.ms/js/vendor/jquery-1.8.0.min.js?v=3.3.27725" type="text/javascript"></script>
<script defer src="https://cdn01.jotfor.ms/js/vendor/maskedinput.min.js?v=3.3.27725" type="text/javascript"></script>
<script defer src="https://cdn02.jotfor.ms/js/vendor/jquery.maskedinput.min.js?v=3.3.27725" type="text/javascript"></script>
<script type="text/javascript">
	JotForm.newDefaultTheme = true;
	JotForm.extendsNewTheme = false;
	JotForm.newPaymentUIForNewCreatedForms = false;
	JotForm.newPaymentUI = true;

	JotForm.init(function() {
		/*INIT-START*/
		if (window.JotForm && JotForm.accessible) $('input_1').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_2').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_4').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_3').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_13').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_14').setAttribute('tabindex', 0);

		JotForm.calendarMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		JotForm.calendarDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
		JotForm.calendarOther = {
			"today": "Today"
		};
		var languageOptions = document.querySelectorAll('#langList li');
		for (var langIndex = 0; langIndex < languageOptions.length; langIndex++) {
			languageOptions[langIndex].on('click', function(e) {
				setTimeout(function() {
					JotForm.setCalendar("15", false, {
						"days": {
							"monday": true,
							"tuesday": true,
							"wednesday": true,
							"thursday": true,
							"friday": true,
							"saturday": true,
							"sunday": true
						},
						"future": true,
						"past": true,
						"custom": false,
						"ranges": false,
						"start": "",
						"end": ""
					});
				}, 0);
			});
		}
		JotForm.onTranslationsFetch(function() {
			JotForm.setCalendar("15", false, {
				"days": {
					"monday": true,
					"tuesday": true,
					"wednesday": true,
					"thursday": true,
					"friday": true,
					"saturday": true,
					"sunday": true
				},
				"future": true,
				"past": true,
				"custom": false,
				"ranges": false,
				"start": "",
				"end": ""
			});
		});
		if (window.JotForm && JotForm.accessible) $('input_17').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_18').setAttribute('tabindex', 0);
		if (window.JotForm && JotForm.accessible) $('input_8').setAttribute('tabindex', 0);
		/*INIT-END*/
	});

	JotForm.prepareCalculationsOnTheFly([null, {
		"name": "name",
		"qid": "1",
		"text": "Name",
		"type": "control_textbox"
	}, {
		"name": "relationshipTo",
		"qid": "2",
		"text": "Relationship to patient",
		"type": "control_textbox"
	}, {
		"name": "email",
		"qid": "3",
		"text": "Email",
		"type": "control_textbox"
	}, {
		"name": "phone",
		"qid": "4",
		"text": "Phone",
		"type": "control_textbox"
	}, null, {
		"description": "",
		"name": "selectTreatment",
		"qid": "6",
		"subLabel": "",
		"text": "Select Treatment Center",
		"type": "control_dropdown"
	}, {
		"name": "bestTime",
		"qid": "7",
		"text": "Best time to call",
		"type": "control_dropdown"
	}, {
		"name": "yourMessage",
		"qid": "8",
		"text": "your Message",
		"type": "control_textarea"
	}, null, {
		"name": "firstName10",
		"qid": "10",
		"text": "Send Message",
		"type": "control_button"
	}, {
		"name": "callerInformation11",
		"qid": "11",
		"text": "Caller information",
		"type": "control_head"
	}, {
		"name": "patientclientInformation",
		"qid": "12",
		"text": "Patient\u002FClient Information",
		"type": "control_head"
	}, {
		"description": "",
		"name": "firstName",
		"qid": "13",
		"subLabel": "",
		"text": "First name",
		"type": "control_textbox"
	}, {
		"description": "",
		"name": "lastName",
		"qid": "14",
		"subLabel": "",
		"text": "last name",
		"type": "control_textbox"
	}, {
		"description": "",
		"name": "dateOf",
		"qid": "15",
		"text": "Date of birth",
		"type": "control_datetime"
	}, {
		"description": "",
		"name": "gender",
		"qid": "16",
		"text": "gender",
		"type": "control_radio"
	}, {
		"description": "",
		"name": "maritalStatus",
		"qid": "17",
		"subLabel": "",
		"text": "marital status",
		"type": "control_textbox"
	}, {
		"description": "",
		"name": "drugsOf",
		"qid": "18",
		"subLabel": "",
		"text": "drugs of choice",
		"type": "control_textbox"
	}, {
		"description": "",
		"name": "howDid19",
		"qid": "19",
		"subLabel": "",
		"text": "How did you hear about AppleGate Recovery?",
		"type": "control_dropdown"
	}, {
		"description": "",
		"name": "pleaseVerify",
		"qid": "20",
		"text": "Please verify that you are human",
		"type": "control_captcha"
	}]);
	setTimeout(function() {
		JotForm.paymentExtrasOnTheFly([null, {
			"name": "name",
			"qid": "1",
			"text": "Name",
			"type": "control_textbox"
		}, {
			"name": "relationshipTo",
			"qid": "2",
			"text": "Relationship to patient",
			"type": "control_textbox"
		}, {
			"name": "email",
			"qid": "3",
			"text": "Email",
			"type": "control_textbox"
		}, {
			"name": "phone",
			"qid": "4",
			"text": "Phone",
			"type": "control_textbox"
		}, null, {
			"description": "",
			"name": "selectTreatment",
			"qid": "6",
			"subLabel": "",
			"text": "Select Treatment Center",
			"type": "control_dropdown"
		}, {
			"name": "bestTime",
			"qid": "7",
			"text": "Best time to call",
			"type": "control_dropdown"
		}, {
			"name": "yourMessage",
			"qid": "8",
			"text": "your Message",
			"type": "control_textarea"
		}, null, {
			"name": "firstName10",
			"qid": "10",
			"text": "Send Message",
			"type": "control_button"
		}, {
			"name": "callerInformation11",
			"qid": "11",
			"text": "Caller information",
			"type": "control_head"
		}, {
			"name": "patientclientInformation",
			"qid": "12",
			"text": "Patient\u002FClient Information",
			"type": "control_head"
		}, {
			"description": "",
			"name": "firstName",
			"qid": "13",
			"subLabel": "",
			"text": "First name",
			"type": "control_textbox"
		}, {
			"description": "",
			"name": "lastName",
			"qid": "14",
			"subLabel": "",
			"text": "last name",
			"type": "control_textbox"
		}, {
			"description": "",
			"name": "dateOf",
			"qid": "15",
			"text": "Date of birth",
			"type": "control_datetime"
		}, {
			"description": "",
			"name": "gender",
			"qid": "16",
			"text": "gender",
			"type": "control_radio"
		}, {
			"description": "",
			"name": "maritalStatus",
			"qid": "17",
			"subLabel": "",
			"text": "marital status",
			"type": "control_textbox"
		}, {
			"description": "",
			"name": "drugsOf",
			"qid": "18",
			"subLabel": "",
			"text": "drugs of choice",
			"type": "control_textbox"
		}, {
			"description": "",
			"name": "howDid19",
			"qid": "19",
			"subLabel": "",
			"text": "How did you hear about AppleGate Recovery?",
			"type": "control_dropdown"
		}, {
			"description": "",
			"name": "pleaseVerify",
			"qid": "20",
			"text": "Please verify that you are human",
			"type": "control_captcha"
		}]);
	}, 20);
</script>
<style type="text/css">
	@media print {
		.form-section {
			display: inline !important
		}

		.form-pagebreak {
			display: none !important
		}

		.form-section-closed {
			height: auto !important
		}

		.page-section {
			position: initial !important
		}
	}
</style>
<link id="custom-font" type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/opt/google-fonts/default/fontface.css" />
<link type="text/css" rel="stylesheet" href="https://cdn01.jotfor.ms/themes/CSS/5e6b428acc8c4e222d1beb91.css?" />
<link type="text/css" rel="stylesheet" href="https://cdn02.jotfor.ms/css/styles/payment/payment_styles.css?3.3.27725" />
<link type="text/css" rel="stylesheet" href="https://cdn03.jotfor.ms/css/styles/payment/payment_feature.css?3.3.27725" />
<style type="text/css" id="form-designer-style">
	/* Injected CSS Code */
	.form-all:after {
		content: "";
		display: table;
		clear: both;
	}

	.form-all {
		font-family: "Open Sans", sans-serif;
	}

	.form-all {
		width: 690px;
	}

	.form-label-left,
	.form-label-right {
		width: 150px;
	}

	.form-label {
		white-space: normal;
	}

	.form-label.form-label-auto {
		display: inline-block;
		float: left;
		text-align: left;
		word-break: break-word;
		width: 150px;
	}

	.form-label-left {
		display: inline-block;
		white-space: normal;
		float: left;
		text-align: left;
	}

	.form-label-right {
		display: inline-block;
		white-space: normal;
		float: left;
		text-align: right;
	}

	.form-label-top {
		white-space: normal;
		display: block;
		float: none;
		text-align: left;
	}

	.form-radio-item label:before {
		top: 0;
	}

	.form-all {
		font-size: 16px;
	}

	.form-label {
		font-weight: bold;
	}

	.form-checkbox-item label,
	.form-radio-item label {
		font-weight: normal;
	}

	.supernova {
		background-color: #ffffff;
		background-color: #f5f5f5;
	}

	.supernova body {
		background-color: transparent;
	}

	/*
@width30: (unit(@formWidth, px) + 60px);
@width60: (unit(@formWidth, px)+ 120px);
@width90: (unit(@formWidth, px)+ 180px);
*/
	/* | */
	@media screen and (min-width: 480px) {
		.supernova .form-all {
			border: 1px solid #dcdcdc;
			box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
		}
	}

	/* | */
	/* | */
	@media screen and (max-width: 480px) {
		.jotform-form .form-all {
			margin: 0;
			width: 100%;
		}
	}

	/* | */
	/* | */
	@media screen and (min-width: 480px) and (max-width: 767px) {
		.jotform-form .form-all {
			margin: 0;
			width: 100%;
		}
	}

	/* | */
	/* | */
	@media screen and (min-width: 480px) and (max-width: 689px) {
		.jotform-form .form-all {
			margin: 0;
			width: 100%;
		}
	}

	/* | */
	/* | */
	@media screen and (min-width: 768px) {
		.jotform-form {
			padding: 60px 0;
		}
	}

	/* | */
	/* | */
	@media screen and (max-width: 689px) {
		.jotform-form .form-all {
			margin: 0;
			width: 100%;
		}
	}

	/* | */
	.supernova .form-all,
	.form-all {
		background-color: #ffffff;
		border: 1px solid transparent;
	}

	.form-header-group {
		border-color: #e6e6e6;
	}

	.form-matrix-table tr {
		border-color: #e6e6e6;
	}

	.form-matrix-table tr:nth-child(2n) {
		background-color: #f2f2f2;
	}

	.form-all {
		color: #3294e5;
	}

	.form-header-group .form-header {
		color: #3294e5;
	}

	.form-header-group .form-subHeader {
		color: #5faceb;
	}

	.form-sub-label {
		color: #5faceb;
	}

	.form-label-top,
	.form-label-left,
	.form-label-right,
	.form-html {
		color: #3294e5;
	}

	.form-checkbox-item label,
	.form-radio-item label {
		color: #5faceb;
	}

	.form-line.form-line-active {
		-webkit-transition-property: all;
		-moz-transition-property: all;
		-ms-transition-property: all;
		-o-transition-property: all;
		transition-property: all;
		-webkit-transition-duration: 0.3s;
		-moz-transition-duration: 0.3s;
		-ms-transition-duration: 0.3s;
		-o-transition-duration: 0.3s;
		transition-duration: 0.3s;
		-webkit-transition-timing-function: ease;
		-moz-transition-timing-function: ease;
		-ms-transition-timing-function: ease;
		-o-transition-timing-function: ease;
		transition-timing-function: ease;
		background-color: #ffffff;
	}

	/* omer */
	.form-radio-item,
	.form-checkbox-item {
		padding-bottom: 0px !important;
	}

	.form-radio-item:last-child,
	.form-checkbox-item:last-child {
		padding-bottom: 0;
	}

	/* omer */
	.form-single-column .form-checkbox-item,
	.form-single-column .form-radio-item {
		width: 100%;
	}

	.form-checkbox-item .editor-container div,
	.form-radio-item .editor-container div {
		position: relative;
	}

	.form-checkbox-item .editor-container div:before,
	.form-radio-item .editor-container div:before {
		display: inline-block;
		vertical-align: middle;
		box-sizing: border-box;
		left: 0;
		width: 18px;
		height: 18px;
	}

	.form-checkbox-item,
	.form-radio-item {
		padding-left: 2px;
	}

	.form-checkbox-item input,
	.form-radio-item input {
		margin-top: 2px;
	}

	.supernova {
		height: 100%;
		background-repeat: no-repeat;
		background-attachment: scroll;
		background-position: center top;
		background-repeat: repeat;
	}

	.supernova {
		background-image: none;
	}

	#stage {
		background-image: none;
	}

	/* | */
	.form-all {
		background-repeat: no-repeat;
		background-attachment: scroll;
		background-position: center top;
		background-repeat: repeat;
	}

	.form-header-group {
		background-repeat: no-repeat;
		background-attachment: scroll;
		background-position: center top;
	}

	.form-line {
		margin-top: 0px;
		margin-bottom: 0px;
	}

	.form-line {
		padding: 12px 20px;
	}

	.form-all .qq-upload-button,
	.form-all .form-submit-button,
	.form-all .form-submit-reset,
	.form-all .form-submit-print {
		font-size: 1em;
		padding: 9px 15px;
		font-family: "Open Sans", sans-serif;
		font-size: 16px;
		font-weight: normal;
	}

	.form-all .form-pagebreak-back,
	.form-all .form-pagebreak-next {
		font-size: 1em;
		padding: 9px 15px;
		font-family: "Open Sans", sans-serif;
		font-size: 16px;
		font-weight: normal;
	}

	/*
& when ( @buttonFontType = google ) {
	@import (css) "@{buttonFontLink}";
}
*/
	h2.form-header {
		line-height: 1.618em;
		font-size: 1.714em;
	}

	h2~.form-subHeader {
		line-height: 1.5em;
		font-size: 1.071em;
	}

	.form-header-group {
		text-align: left;
	}

	/*.form-dropdown,
.form-radio-item,
.form-checkbox-item,
.form-radio-other-input,
.form-checkbox-other-input,*/
	.form-captcha input,
	.form-spinner input,
	.form-error-message {
		padding: 4px 3px 2px 3px;
	}

	.form-header-group {
		font-family: "Open Sans", sans-serif;
	}

	.form-section {
		padding: 0px 0px 0px 0px;
	}

	.form-header-group {
		margin: 12px 36px 12px 36px;
	}

	.form-header-group {
		padding: 24px 0px 24px 0px;
	}

	.form-textbox,
	.form-textarea {
		padding: 4px 3px 2px 3px;
	}

	.form-textbox,
	.form-textarea,
	.form-radio-other-input,
	.form-checkbox-other-input,
	.form-captcha input,
	.form-spinner input {
		background-color: #ffffff;
	}

	.form-textbox {
		height: 30px;
	}

	.form-matrix-row-headers,
	.form-matrix-column-headers,
	.form-matrix-values {
		padding: 4px;
	}

	[data-type="control_dropdown"] .form-input,
	[data-type="control_dropdown"] .form-input-wide {
		width: 150px;
	}

	.form-buttons-wrapper {
		margin-left: 0 !important;
		text-align: center;
	}

	.form-label {
		font-family: "Open Sans", sans-serif;
	}

	li[data-type="control_image"] div {
		text-align: left;
	}

	li[data-type="control_image"] img {
		border: none;
		border-width: 0px !important;
		border-style: solid !important;
		border-color: false !important;
	}

	.form-line-column {
		width: auto;
	}

	.form-line-error {
		overflow: hidden;
		-webkit-transition-property: none;
		-moz-transition-property: none;
		-ms-transition-property: none;
		-o-transition-property: none;
		transition-property: none;
		-webkit-transition-duration: 0.3s;
		-moz-transition-duration: 0.3s;
		-ms-transition-duration: 0.3s;
		-o-transition-duration: 0.3s;
		transition-duration: 0.3s;
		-webkit-transition-timing-function: ease;
		-moz-transition-timing-function: ease;
		-ms-transition-timing-function: ease;
		-o-transition-timing-function: ease;
		transition-timing-function: ease;
		background-color: #ffffff;
	}

	.form-line-error .form-error-message {
		background-color: #ff3200;
		clear: both;
		float: none;
	}

	.form-line-error .form-error-message .form-error-arrow {
		border-bottom-color: #ff3200;
	}

	.form-line-error input:not(#coupon-input),
	.form-line-error textarea,
	.form-line-error .form-validation-error {
		border: 1px solid #ff3200;
		box-shadow: 0 0 3px #ff3200;
	}

	.ie-8 .form-all {
		margin-top: auto;
		margin-top: initial;
	}

	.ie-8 .form-all:before {
		display: none;
	}

	[data-type="control_clear"] {
		display: none;
	}

	/* | */
	@media screen and (max-width: 480px),
	screen and (max-device-width: 767px) and (orientation: portrait),
	screen and (max-device-width: 415px) and (orientation: landscape) {
		.testOne {
			letter-spacing: 0;
		}

		.form-all {
			border: 0;
			max-width: initial;
		}

		.form-sub-label-container {
			width: 100%;
			margin: 0;
			margin-right: 0;
			float: left;
			box-sizing: border-box;
		}

		span.form-sub-label-container+span.form-sub-label-container {
			margin-right: 0;
		}

		.form-sub-label {
			white-space: normal;
		}

		.form-address-table td,
		.form-address-table th {
			padding: 0 1px 10px;
		}

		.form-submit-button,
		.form-submit-print,
		.form-submit-reset {
			width: 100%;
			margin-left: 0 !important;
		}

		div[id*=at_] {
			font-size: 14px;
			font-weight: 700;
			height: 8px;
			margin-top: 6px;
		}

		.showAutoCalendar {
			width: 20px;
		}

		img.form-image {
			max-width: 100%;
			height: auto;
		}

		.form-matrix-row-headers {
			width: 100%;
			word-break: break-all;
			min-width: 80px;
		}

		.form-collapse-table,
		.form-header-group {
			margin: 0;
		}

		.form-collapse-table {
			height: 100%;
			display: inline-block;
			width: 100%;
		}

		.form-collapse-hidden {
			display: none !important;
		}

		.form-input {
			width: 100%;
		}

		.form-label {
			width: 100% !important;
		}

		.form-label-left,
		.form-label-right {
			display: block;
			float: none;
			text-align: left;
			width: auto !important;
		}

		.form-line,
		.form-line.form-line-column {
			padding: 2% 5%;
			box-sizing: border-box;
		}

		input[type=text],
		input[type=email],
		input[type=tel],
		textarea {
			width: 100%;
			box-sizing: border-box;
			max-width: initial !important;
		}

		.form-radio-other-input,
		.form-checkbox-other-input {
			max-width: 55% !important;
		}

		.form-dropdown,
		.form-textarea,
		.form-textbox {
			width: 100% !important;
			box-sizing: border-box;
		}

		.form-input,
		.form-input-wide,
		.form-textarea,
		.form-textbox,
		.form-dropdown {
			max-width: initial !important;
		}

		.form-checkbox-item:not(#foo),
		.form-radio-item:not(#foo) {
			width: 100%;
		}

		.form-address-city,
		.form-address-line,
		.form-address-postal,
		.form-address-state,
		.form-address-table,
		.form-address-table .form-sub-label-container,
		.form-address-table select,
		.form-input {
			width: 100%;
		}

		div.form-header-group {
			padding: 24px 0px !important;
			margin: 0 12px 2% !important;
			margin-left: 5% !important;
			margin-right: 5% !important;
			box-sizing: border-box;
		}

		div.form-header-group.hasImage img {
			max-width: 100%;
		}

		[data-type="control_button"] {
			margin-bottom: 0 !important;
		}

		[data-type=control_fullname] .form-sub-label-container {
			width: 48%;
		}

		[data-type=control_fullname] .form-sub-label-container:first-child {
			margin-right: 4%;
		}

		[data-type=control_phone] .form-sub-label-container {
			width: 65%;
			margin-right: 0;
			margin-left: 0;
			float: left;
		}

		[data-type=control_phone] .form-sub-label-container:first-child {
			width: 31%;
			margin-right: 4%;
		}

		[data-type=control_datetime] .allowTime-container {
			width: 100%;
		}

		[data-type=control_datetime] .allowTime-container .form-sub-label-container {
			width: 24% !important;
			margin-left: 6%;
			margin-right: 0;
		}

		[data-type=control_datetime] .allowTime-container .form-sub-label-container:first-child {
			margin-left: 0;
		}

		[data-type=control_datetime] span+span+span>span:first-child {
			display: block;
			width: 100% !important;
		}

		[data-type=control_birthdate] .form-sub-label-container,
		[data-type=control_time] .form-sub-label-container {
			width: 27.3% !important;
			margin-right: 6% !important;
		}

		[data-type=control_time] .form-sub-label-container:last-child {
			width: 33.3% !important;
			margin-right: 0 !important;
		}

		.form-pagebreak-back-container,
		.form-pagebreak-next-container {
			min-height: 1px;
			width: 50% !important;
		}

		.form-pagebreak-back,
		.form-pagebreak-next,
		.form-product-item.hover-product-item {
			width: 100%;
		}

		.form-pagebreak-back-container {
			padding: 0;
			text-align: right;
		}

		.form-pagebreak-next-container {
			padding: 0;
			text-align: left;
		}

		.form-pagebreak {
			margin: 0 auto;
		}

		.form-buttons-wrapper {
			margin: 0 !important;
			margin-left: 0 !important;
		}

		.form-buttons-wrapper button {
			width: 100%;
		}

		.form-buttons-wrapper .form-submit-print {
			margin: 0 !important;
		}

		table {
			width: 100% !important;
			max-width: initial !important;
		}

		table td+td {
			padding-left: 3%;
		}

		.form-checkbox-item,
		.form-radio-item {
			white-space: normal !important;
		}

		.form-checkbox-item input,
		.form-radio-item input {
			width: auto;
		}

		.form-collapse-table {
			margin: 0 5%;
			display: block;
			zoom: 1;
			width: auto;
		}

		.form-collapse-table:before,
		.form-collapse-table:after {
			display: table;
			content: '';
			line-height: 0;
		}

		.form-collapse-table:after {
			clear: both;
		}

		.fb-like-box {
			width: 98% !important;
		}

		.form-error-message {
			clear: both;
			bottom: -10px;
		}

		.date-separate,
		.phone-separate {
			display: none;
		}

		.custom-field-frame,
		.direct-embed-widgets,
		.signature-pad-wrapper {
			width: 100% !important;
		}
	}

	/* | */

	/*__INSPECT_SEPERATOR__*/
	body,
	.supernova {
		margin: 0;
		padding: 0;
		background: #ffffff !important;
	}

	.jotform-form {
		padding: 0 !important;
	}

	.supernova .form-all,
	.form-all {
		background-color: #ffffff;
		border: 1px solid transparent;
		box-shadow: none !important;
		border-radius: 0 !important;
		margin: 0 auto !important;
	}

	ul.form-section {
		padding: 0;
	}

	.form-section div.form-header-group.header-large {
		padding: 0;
		margin: 0;
	}

	.form-label.form-label-left {
		text-transform: uppercase;
	}

	.form-line {
		padding: 0 !important;
		margin: 10px 0 !important;
	}

	.form-label {
		width: 25% !important;
		margin-right: 0 !important;
		padding-right: 15px;
	}

	.form-required {
		color: #3294e5 !important;
	}

	.form-input {
		width: 60% !important;
	}

	.form-dropdown {
		width: 100% !important;
	}

	.form-header-group .form-header {
		font-weight: 700;
		color: #1a4283;
		margin-top: 24px;
		font-size: 22px;
		padding-bottom: 8px;
	}

	#label_6,
	#label_20,
	#label_19 {
		visibility: hidden;
		height: 0;
	}

	#label_7 {
		visibility: hidden;
	}

	.form-error-message {
		padding: 4px 3px 2px 18px !important;
	}

	.form-dropdown:hover,
	.form-textarea:hover,
	.form-textbox:hover,
	.signature-wrapper:hover,
	.form-line-error input:not(#coupon-input),
	.form-line-error textarea,
	.form-line-error .form-validation-error,
	.form-dropdown:focus,
	.form-textarea:focus,
	.form-textbox:focus,
	.signature-wrapper:focus {
		box-shadow: none !important;
	}

	.form-dropdown,
	.form-textarea,
	.form-textbox {
		height: auto !important;
		padding-top: 10px !important;
		padding-bottom: 10px !important;
		padding-left: 10px !important;
		padding-right: 10px !important;
		border-radius: 0 !important;
		box-shadow: none;
		box-shadow: none;
		outline: none;
		line-height: 1.3;
		text-align: left;
		color: #808080 !important;
		background: #ffffff;
		font-size: 16px;
		font-weight: 400;
		border-color: #b3b3b3 !important;
	}

	.form-textarea {
		height: 120px !important;
		resize: unset !important;
	}

	.form-submit-button.submit-button {
		transition: all 0.3s;
		border-radius: 45px;
		background: #3294e5;
		border-color: #3294e5;
		color: #ffffff;
		padding-top: 12px;
		padding-bottom: 12px;
		width: 220px;
		min-height: 45px;
		text-transform: uppercase;
		font-weight: 700;
		box-shadow: none;
	}

	.form-submit-button.submit-button:hover {
		color: #3294e5;
		background: #ffffff;
		box-shadow: none;
	}

	.page-section li[data-type=control_button] {
		background: transparent !important;
	}

	.form-buttons-wrapper {
		border: 0;
	}

	#sublabel_15_litemode {
		display: none;
	}

	@media screen and (max-width: 560px) {
		ul.form-section {
			padding: 0 !important;
		}

		.form-label {
			margin-bottom: 0 !important;
			width: 100% !important;
		}

		.form-input {
			width: 100% !important;
			margin-top: 12px !important;
		}

		.form-line {
			padding: 0 !important;
			margin: 12px 0 0 !important;
		}

		#label_6,
		#label_7 {
			display: none;
		}

		.form-section div.form-header-group.header-large {
			padding: 0 !important;
			margin: 0 !important;
		}

	}

	.form-line-error .form-error-message {
		display: none;
	}


	/* Injected CSS Code */
</style>

<form class="jotform-form" action="https://submit.jotform.com/submit/211465860356458/" method="post" name="form_211465860356458" id="211465860356458" accept-charset="utf-8" autocomplete="on">
	<input type="hidden" name="formID" value="211465860356458" />
	<input type="hidden" id="JWTContainer" value="" />
	<input type="hidden" id="cardinalOrderNumber" value="" />
	<div role="main" class="form-all">
		<style>
			.form-all:before {
				background: none;
			}
		</style>
		<ul class="form-section page-section">
			<li id="cid_11" class="form-input-wide" data-type="control_head">
				<div class="form-header-group  header-large">
					<div class="header-text httal htvam">
						<h1 id="header_11" class="form-header" data-component="header">
							Caller information
						</h1>
					</div>
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_1">
				<label class="form-label form-label-left form-label-auto" id="label_1" for="input_1">
					Name
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_1" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_1" name="q1_name" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_1"
						required="" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_2">
				<label class="form-label form-label-left form-label-auto" id="label_2" for="input_2">
					Relationship to patient
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_2" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_2" name="q2_relationshipTo" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox"
						aria-labelledby="label_2" required="" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_4">
				<label class="form-label form-label-left form-label-auto" id="label_4" for="input_4">
					Phone
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_4" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_4" name="q4_phone" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_4"
						required="" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_dropdown" id="id_6">
				<label class="form-label form-label-left form-label-auto" id="label_6" for="input_6">
					Select Treatment Center
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_6" class="form-input jf-required" data-layout="half">
					<select class="form-dropdown validate[required]" style="width:150px;" id="input_6" name="q6_selectTreatment">
						<option value=""> Select Treatment Center * </option>
						<option value="Baton Rouge"> Baton Rouge </option>
						<option value="Bossier City"> Bossier City </option>
						<option value="Bowling Green"> Bowling Green </option>
						<option value="Camden"> Camden </option>
						<option value="Charleston"> Charleston </option>
						<option value="Crestview Hills"> Crestview Hills </option>
						<option value="Dickson"> Dickson </option>
						<option value="Dyersburg"> Dyersburg </option>
						<option value="El Dorado"> El Dorado </option>
						<option value="Elyria"> Elyria </option>
						<option value="Huber Heights"> Huber Heights </option>
						<option value="Jackson"> Jackson </option>
						<option value="Lake Charles"> Lake Charles </option>
						<option value="Lewisville"> Lewisville </option>
						<option value="Mansfield"> Mansfield </option>
						<option value="Martinsburg"> Martinsburg </option>
						<option value="Metairie"> Metairie </option>
						<option value="Middletown"> Middletown </option>
						<option value="Monroe"> Monroe </option>
						<option value="Monroeville"> Monroeville </option>
						<option value="Mt. Carmel"> Mt. Carmel </option>
						<option value="Nashville"> Nashville </option>
						<option value="Oakdale"> Oakdale </option>
						<option value="Plano"> Plano </option>
						<option value="Port Matilda"> Port Matilda </option>
						<option value="Richmond"> Richmond </option>
						<option value="Savannah"> Savannah </option>
						<option value="Selinsgrove"> Selinsgrove </option>
						<option value="Selmer"> Selmer </option>
						<option value="Slidell"> Slidell </option>
						<option value="Thibodaux"> Thibodaux </option>
						<option value="Washington - Locust Ave"> Washington - Locust Ave </option>
						<option value="Washington - N. Main St."> Washington - N. Main St. </option>
						<option value="Williamsport"> Williamsport </option>
						<option value="Willoughby"> Willoughby </option>
					</select>
				</div>
			</li>
			<li class="form-line" data-type="control_dropdown" id="id_7">
				<label class="form-label form-label-left form-label-auto" id="label_7" for="input_7"> Best time to call </label>
				<div id="cid_7" class="form-input" data-layout="half">
					<select class="form-dropdown" style="width:150px;" id="input_7" name="q7_bestTime">
						<option value=""> Best time to call </option>
						<option value="Morning (before 12 PM)"> Morning (before 12 PM) </option>
						<option value="Afternoon (12 PM - 5 PM)"> Afternoon (12 PM - 5 PM) </option>
						<option value="Night (after 5 PM)"> Night (after 5 PM) </option>
					</select>
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_3">
				<label class="form-label form-label-left form-label-auto" id="label_3" for="input_3">
					Email
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_3" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_3" name="q3_email" data-type="input-textbox" class="form-textbox validate[required, Email]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_3"
						required="" />
				</div>
			</li>
			<li id="cid_12" class="form-input-wide" data-type="control_head">
				<div class="form-header-group  header-large">
					<div class="header-text httal htvam">
						<h1 id="header_12" class="form-header" data-component="header">
							Patient/Client Information
						</h1>
					</div>
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_13">
				<label class="form-label form-label-left form-label-auto" id="label_13" for="input_13">
					First name
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_13" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_13" name="q13_firstName" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_13"
						required="" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_14">
				<label class="form-label form-label-left form-label-auto" id="label_14" for="input_14">
					last name
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_14" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_14" name="q14_lastName" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_14"
						required="" />
				</div>
			</li>
			<li class="form-line" data-type="control_datetime" id="id_15">
				<label class="form-label form-label-left form-label-auto" id="label_15" for="lite_mode_15"> Date of birth </label>
				<div id="cid_15" class="form-input" data-layout="half">
					<div data-wrapper-react="true">
						<div style="display:none">
							<span class="form-sub-label-container" style="vertical-align:top">
								<input type="tel" class="form-textbox validate[limitDate]" id="month_15" name="q15_dateOf[month]" size="2" data-maxlength="2" data-age="" maxLength="2" value="" autoComplete="off"
									aria-labelledby="label_15 sublabel_15_month" />
								<span class="date-separate" aria-hidden="true">
									-
								</span>
								<label class="form-sub-label" for="month_15" id="sublabel_15_month" style="min-height:13px" aria-hidden="false"> Month </label>
							</span>
							<span class="form-sub-label-container" style="vertical-align:top">
								<input type="tel" class="form-textbox validate[limitDate]" id="day_15" name="q15_dateOf[day]" size="2" data-maxlength="2" data-age="" maxLength="2" value="" autoComplete="off"
									aria-labelledby="label_15 sublabel_15_day" />
								<span class="date-separate" aria-hidden="true">
									-
								</span>
								<label class="form-sub-label" for="day_15" id="sublabel_15_day" style="min-height:13px" aria-hidden="false"> Day </label>
							</span>
							<span class="form-sub-label-container" style="vertical-align:top">
								<input type="tel" class="form-textbox validate[limitDate]" id="year_15" name="q15_dateOf[year]" size="4" data-maxlength="4" data-age="" maxLength="4" value="" autoComplete="off"
									aria-labelledby="label_15 sublabel_15_year" />
								<label class="form-sub-label" for="year_15" id="sublabel_15_year" style="min-height:13px" aria-hidden="false"> Year </label>
							</span>
						</div>
						<span class="form-sub-label-container" style="vertical-align:top">
							<input type="text" class="form-textbox validate[limitDate, validateLiteDate]" id="lite_mode_15" size="12" data-maxlength="12" maxLength="12" data-age="" value="" data-format="mmddyyyy" data-seperator="-"
								placeholder="MM-DD-YYYY" autoComplete="off" aria-labelledby="label_15 sublabel_15_litemode" />
							<img class=" newDefaultTheme-dateIcon icon-liteMode" alt="Pick a Date" id="input_15_pick" src="https://cdn.jotfor.ms/images/calendar.png" data-component="datetime" aria-hidden="true" data-allow-time="No"
								data-version="v2" />
							<label class="form-sub-label" for="lite_mode_15" id="sublabel_15_litemode" style="min-height:13px" aria-hidden="false"> Date </label>
						</span>
					</div>
				</div>
			</li>
			<li class="form-line" data-type="control_radio" id="id_16">
				<label class="form-label form-label-left form-label-auto" id="label_16" for="input_16"> gender </label>
				<div id="cid_16" class="form-input" data-layout="full">
					<div class="form-single-column" role="group" aria-labelledby="label_16" data-component="radio">
						<span class="form-radio-item" style="clear:left">
							<span class="dragger-item">
							</span>
							<input type="radio" aria-describedby="label_16" class="form-radio" id="input_16_0" name="q16_gender" value="Male" />
							<label id="label_input_16_0" for="input_16_0"> Male </label>
						</span>
						<span class="form-radio-item" style="clear:left">
							<span class="dragger-item">
							</span>
							<input type="radio" aria-describedby="label_16" class="form-radio" id="input_16_1" name="q16_gender" value="Female" />
							<label id="label_input_16_1" for="input_16_1"> Female </label>
						</span>
					</div>
				</div>
			</li>
			<li class="form-line" data-type="control_textbox" id="id_17">
				<label class="form-label form-label-left form-label-auto" id="label_17" for="input_17"> marital status </label>
				<div id="cid_17" class="form-input" data-layout="half">
					<input type="text" id="input_17" name="q17_maritalStatus" data-type="input-textbox" class="form-textbox" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_17" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textbox" id="id_18">
				<label class="form-label form-label-left form-label-auto" id="label_18" for="input_18">
					drugs of choice
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_18" class="form-input jf-required" data-layout="half">
					<input type="text" id="input_18" name="q18_drugsOf" data-type="input-textbox" class="form-textbox validate[required]" data-defaultvalue="" style="width:20px" size="20" value="" data-component="textbox" aria-labelledby="label_18"
						required="" />
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_textarea" id="id_8">
				<label class="form-label form-label-left form-label-auto" id="label_8" for="input_8">
					your Message
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_8" class="form-input jf-required" data-layout="full">
					<textarea id="input_8" class="form-textarea validate[required]" name="q8_yourMessage" style="width:50px;height:20px" data-component="textarea" required="" aria-labelledby="label_8"></textarea>
				</div>
			</li>
			<li class="form-line" data-type="control_dropdown" id="id_19">
				<label class="form-label form-label-left form-label-auto" id="label_19" for="input_19"> How did you hear about AppleGate Recovery? </label>
				<div id="cid_19" class="form-input" data-layout="half">
					<select class="form-dropdown" style="width:150px;" id="input_19" name="q19_howDid19">
						<option value=""> How did you hear about AppleGate Recovery? </option>
						<option value="Alumni"> Alumni </option>
						<option value="Professional Referral"> Professional Referral </option>
						<option value="Doctor/Hospital"> Doctor/Hospital </option>
						<option value="Friend/Word of mouth"> Friend/Word of mouth </option>
						<option value="SAMHSA Site"> SAMHSA Site </option>
						<option value="Search Engine (Google, Bing)"> Search Engine (Google, Bing) </option>
						<option value="Suboxone.com"> Suboxone.com </option>
					</select>
				</div>
			</li>
			<li class="form-line jf-required" data-type="control_captcha" id="id_20">
				<label class="form-label form-label-left form-label-auto" id="label_20" for="input_20">
					Please verify that you are human
					<span class="form-required">
						*
					</span>
				</label>
				<div id="cid_20" class="form-input jf-required" data-layout="full">
					<section data-wrapper-react="true">
						<div id="recaptcha_input_20" data-component="recaptcha" data-callback="recaptchaCallbackinput_20" data-expired-callback="recaptchaExpiredCallbackinput_20">
						</div>
						<input type="hidden" id="input_20" class="hidden validate[required]" name="recaptcha_visible" required="" />
						<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=explicit&amp;onload=recaptchaLoadedinput_20"></script>
						<script type="text/javascript">
							var recaptchaLoadedinput_20 = function() {
								window.grecaptcha.render($("recaptcha_input_20"), {
									sitekey: '6LdU3CgUAAAAAB0nnFM3M3T0sy707slYYU51RroJ',
								});
								var grecaptchaBadge = document.querySelector('.grecaptcha-badge');
								if (grecaptchaBadge) {
									grecaptchaBadge.style.boxShadow = 'gray 0px 0px 2px';
								}
							};

							/**
							 * Called when the reCaptcha verifies the user is human
							 * For invisible reCaptcha;
							 *   Submit event is stopped after validations and recaptcha is executed.
							 *   If a challenge is not displayed, this will be called right after grecaptcha.execute()
							 *   If a challenge is displayed, this will be called when the challenge is solved successfully
							 *   Submit is triggered to actually submit the form since it is stopped before.
							 */
							var recaptchaCallbackinput_20 = function() {
								var isInvisibleReCaptcha = false;
								var hiddenInput = $("input_20");
								hiddenInput.setValue(1);
								if (!isInvisibleReCaptcha) {
									if (hiddenInput.validateInput) {
										hiddenInput.validateInput();
									}
								} else {
									triggerSubmit(hiddenInput.form)
								}

								function triggerSubmit(formElement) {
									var button = formElement.ownerDocument.createElement('input');
									button.style.display = 'none';
									button.type = 'submit';
									formElement.appendChild(button).click();
									formElement.removeChild(button);
								}
							}

							// not really required for invisible recaptcha
							var recaptchaExpiredCallbackinput_20 = function() {
								var hiddenInput = $("input_20");
								hiddenInput.writeAttribute("value", false);
								if (hiddenInput.validateInput) {
									hiddenInput.validateInput();
								}
							}
						</script>
					</section>
				</div>
			</li>
			<li class="form-line" data-type="control_button" id="id_10">
				<div id="cid_10" class="form-input-wide" data-layout="full">
					<div data-align="center" class="form-buttons-wrapper form-buttons-center   jsTest-button-wrapperField">
						<button id="input_10" type="submit" class="form-submit-button submit-button jf-form-buttons jsTest-submitField" data-component="button" data-content="">
							Send Message
						</button>
					</div>
				</div>
			</li>
			<li style="display:none">
				Should be Empty:
				<input type="text" name="website" value="" />
			</li>
		</ul>
	</div>
	<script>
		JotForm.showJotFormPowered = "0";
	</script>
	<script>
		JotForm.poweredByText = "Powered by JotForm";
	</script>
	<input type="hidden" class="simple_spc" id="simple_spc" name="simple_spc" value="211465860356458" />
	<script type="text/javascript">
		var all_spc = document.querySelectorAll("form[id='211465860356458'] .si" + "mple" + "_spc");
		for (var i = 0; i < all_spc.length; i++) {
			all_spc[i].value = "211465860356458-211465860356458";
		}
	</script>
</form>
