<?php $template_url = get_template_directory_uri(); ?>
<!-- MastHead -->
<div class="masthead">
    <div class="container wow hide--wow animate__animated animate__fadeInDown">
        <?php if( is_category() ): ?>
            <?php $category = get_term( get_query_var('cat'), 'category' );  ?>
            <h1 class="text-color--white text-center"><?php echo $category->name; ?></h1>
        <?php elseif( is_archive() ): ?>
            <h1 class="text-color--white text-center"><?php echo post_type_archive_title( '', false ); ?></h1>
        <?php elseif( is_search() ): ?>
            <h1 class="text-color--white text-center">Search Results</h1>
        <?php elseif( is_404() ): ?>
            <h1 class="text-color--white text-center">404: Oops! We can't find the page you're looking for.</h1>
        <?php else: ?>
            <h1 class="text-color--white text-center"><?php echo get_the_title(); ?></h1>
        <?php endif; ?>
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<ul class="breadcrumbs">','</ul>' );
        }
        ?>
    </div>
    <!-- MastHead Outline -->
    <?php
        $outline = get_field('masthead_background');
    ?>

    <?php if( strcmp($outline, 'gainsboro') == 0 ): ?>
        <img class="masthead__outline" src="<?php echo $template_url; ?>/assets/img/masthead-outline-bg.svg" alt="MastHead Outline">
    <?php else: ?>
        <img class="masthead__outline" src="<?php echo $template_url; ?>/assets/img/masthead-outline.svg" alt="MastHead Outline">
    <?php endif; ?>
</div>
