<li class="guides-item">
    <?php
        $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
        $image_alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
    ?>
    <img class="guides__img visible-lg visible-md hidden-sm-down" src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
    <div class="guides-item-wrapper">
        <div class="guides-content">
            <h3><?php echo get_the_title(); ?></h3>
            <p><?php echo get_field('descriptions', get_the_ID()); ?></p>
        </div>
        <a class="guides__link" href="">
            <span class="guides__link-circle">
                <img src="<?php echo $template_url; ?>/assets/icons/guides-icon.svg" alt="">
            </span>
            Download
        </a>
    </div>
</li>
