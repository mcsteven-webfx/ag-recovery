<?php
/**
 * If your form is generated using get_search_form() you do not need to do this,
 * as SearchWP Live Search does it automatically out of the box
 */
?>

<?php $template_url = get_template_directory_uri(); ?>
<!-- Search Bar -->
<form class="header-bar" method="get" action="<?php echo home_url(); ?>" role="search">
    <div class="header-bar-wrapper">
        <img class="header-bar__icon" src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
        <input class="header-bar__search" type="text" name="s" id="s" placeholder="Search" data-swplive="true" >
        <button type="submit" name="submit" class="header-bar__link bg-color--blue" href="./">
            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
        </button>
    </div>
</form>
