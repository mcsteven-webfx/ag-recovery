const elBtn = document.querySelector('.js-read-more-expand'),
		elContent	= document.querySelector('.js-read-more')
	if( null !== elBtn && null !== elContent ) {
		elBtn.addEventListener( 'click', () => {
			const state = elContent.classList.contains('is-expanded')
			if( state ) {
				elBtn.innerText = 'Expand'
				elContent.classList.remove('is-expanded')
			} else {
				elBtn.innerText = 'Collapse'
				elContent.classList.add('is-expanded')
			}
		})
	}