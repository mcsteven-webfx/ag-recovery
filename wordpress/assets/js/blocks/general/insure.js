var FX = ( function( FX, $ ) {

    $( () => {
        FX.InsureSlider.init()
    })

    FX.InsureSlider = {
        $slider: null,

        init() {
            this.$slider = $('.insure-slider');

            if( this.$slider.length ) {
                this.applySlick()
            }
        },

        applySlick() {
            this.$slider.slick({
                nextArrow: $('.js-next'),
                prevArrow: $('.js-prev'),
                dots: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )
