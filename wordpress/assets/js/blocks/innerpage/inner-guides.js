var FX = ( function( FX, $ ) {

    $( () => {
        FX.Guides.init()
    })

    FX.Guides = {
        init() {
            initialLoading('guides', '', '1');

            $('.search-bar__search').keypress(function(e) {
                // Enter pressed?
                if(e.which == 10 || e.which == 13) {
                    $('.js-search').trigger("click");
                }
            });

            $('.js-filter').click(function() {
                $("input[name='search']").val('');

                var $filter = $(this).attr('data-filter');
                var $span = $(this).find('.guides__btn-circle');
                var $page = $('.guides-box').attr('data-page');

                //Remove Active Class on all filters
                $('.guides__btn-circle').removeClass('active');
                //Add Active Class for the Clicked filters
                $span.addClass('active');

                //Ajax!!!
                initialLoading($filter, '', $page);
            });

            $('.js-search').click(function(e) {
                e.preventDefault();
                var $title = $("input[name='search']").val();
                var $parent = $(this).parents('.search-bar');
                var $filter = $('.guides__btn-circle.active').parent().attr('data-filter');
                var $page = $('.guides-box').attr('data-page');

                if($title != '') {
                    //Hide if Search has value
                    $parent.find('.search__error').hide();

                    //Ajax!!!
                    initialLoading($filter, $title, $page);
                } else {
                    //Show if Search has no inputted fields
                    $parent.find('.search__error').show();
                }

            });

            $(document).on('click', '.js-link', function(e){
                e.preventDefault();
                var $page = $(this).attr('data-pagenum');
                var $filter = $('.guides__btn-circle.active').parent().attr('data-filter');

                initialLoading($filter, '', $page);
            });

            function initialLoading($filter, $title, $page) {
                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'filter_guides',
                        title: $title,
                        category: $filter,
                        page: $page,
                    },
                    success: function(res) {
                        $('.guides-ajax').html(res);
                    },
                    beforeSend: function(){
                        $('.guides-box').addClass('custom-spinner');
                    },
                    complete: function(){
                        $('.guides-box').removeClass('custom-spinner');
                    }
                })
            }
        },


    }

    return FX

} ( FX || {}, jQuery ) )
