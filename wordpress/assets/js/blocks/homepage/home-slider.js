var FX = ( function( FX, $ ) {

    $( () => {
        FX.HomeSlider.init()
    })

    FX.HomeSlider = {
        $slider: null,

        init() {
            this.$slider = $('.home-slide');

            if( this.$slider.length ) {
                this.applySlick()
            }
        },

        applySlick() {
            this.$slider.slick({
                arrows: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                customPaging: function(slick,index) {
                    return '<span class="home-slick-dots"></span>';
                }
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )
