var FX = ( function( FX, $ ) {

    $( () => {
        FX.HomeResource.init()
    })

    FX.HomeResource = {
        init() {
            var $button = $('.js-resource');

            $button.click(function(){
                //Remove all active class
                $button.removeClass('active');
                $('.js-resource-tab').removeClass('active');
                $('.js-resource-tab').children().removeClass('animate__animated animate__fadeIn');
                $('.js-resource-link').removeClass('active');

                //Target only the clicked element
                $(this).addClass('active');
                var $tab = $(this).attr("data-tab");

                $("[data-target='"+ $tab +"']").addClass("active");
                $("[data-target='"+ $tab +"']").children().addClass("animate__animated animate__fadeIn");
            });
        },
    }

    return FX

} ( FX || {}, jQuery ) )
