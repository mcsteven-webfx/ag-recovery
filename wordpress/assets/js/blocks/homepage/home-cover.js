var FX = ( function( FX, $ ) {

    $( () => {
        FX.HomeCoverSlider.init()
    })

    FX.HomeCoverSlider = {
        $slider01: null,

        init() {
            this.$slider01 = $('.home-cover-slider');

            if( this.$slider01.length ) {
                this.applySlick()
            }
        },

        applySlick() {
            this.$slider01.slick({
                nextArrow: $('.js-next'),
                prevArrow: $('.js-prev'),
                dots: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        }
    }

    return FX

} ( FX || {}, jQuery ) )
