var FX = ( function( FX, $ ) {

    $( () => {
        FX.FAQS.init()
    })

    FX.FAQS = {
        init() {
            initialLoading('general-faqs');

            $('.js-filter').click(function() {

                var $filter = $(this).attr('data-filter');
                var $span = $(this).find('.faqs__btn-circle');

                //Remove Active Class on all filters
                $('.faqs__btn-circle').removeClass('active');
                //Add Active Class for the Clicked filters
                $span.addClass('active');

                //Ajax!!!
                initialLoading($filter);
            });

            function initialLoading($filter) {
                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'filter_faqs',
                        filter: $filter,
                    },
                    success: function(res) {
                        $('.fxa-accordion__panels').html(res);

                        document.querySelectorAll('.js-accordion').forEach( el => {
                            new FxAccordion( el )
                        })
                    },
                    beforeSend: function(){
                        $('.fxa-accordion__panels').addClass('custom-spinner');
                    },
                    complete: function(){
                        $('.fxa-accordion__panels').removeClass('custom-spinner');
                    }
                })
            }
        },


    }

    return FX

} ( FX || {}, jQuery ) )
