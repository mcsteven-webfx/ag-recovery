var FX = ( function( FX, $ ) {

    $( () => {
        FX.Blogs.init()
    })

    FX.Blogs = {

        init() {
            var $category = $('.blogs-wrapper').attr("data-cat");

            initialLoading($category, '', '1');

            $('.search-bar__search').keypress(function(e) {
                // Enter pressed?
                if(e.which == 10 || e.which == 13) {
                    $('.js-search').trigger("click");
                }
            });

            $('.js-search').click(function(e) {
                e.preventDefault();
                var $title = $("input[name='search']").val();
                var $parent = $(this).parents('.search-bar');


                if($title != '') {
                    //Hide if Search has value
                    $parent.find('.search__error').hide();
                    //Ajax!!!
                    initialLoading($category, $title, '1');
                } else {
                    //Show if Search has no inputted fields
                    $parent.find('.search__error').show();
                }
            });

            $(document).on('click', '.js-link', function(e){
                e.preventDefault();
                var $page = $(this).attr('data-pagenum');

                initialLoading($category, '', $page);
            });

            function initialLoading($category, $title, $page) {
                console.log('running');

                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'filter_blogs',
                        title: $title,
                        category: $category,
                        page: $page,
                    },
                    success: function(res) {
                        $('.blogs-main').html(res);
                    },
                    beforeSend: function(){
                        $('.blogs-main').addClass('custom-spinner');
                    },
                    complete: function(){
                        $('.blogs-main').removeClass('custom-spinner');
                    }
                })
            }
        }
    }

    return FX

} ( FX || {}, jQuery ) )
