jQuery( function () {
    tinymce.PluginManager.add('mce_editor_js', function(editor) {
        // Button Dropdown
        editor.addButton('editor_buttons_dropdown', {
            text: 'Buttons',
            tooltip: 'Add Button',
            type: 'menubutton',
            menu: [
                {
                    text: 'Blue ( Border Only )',
                    tooltip: 'Hover: Background Blue',
                    value: '<a class="btn-outlined btn-outlined-tertiary" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                },
                {
                    text: 'White ( Background )',
                    tooltip: 'Hover: Background Orange',
                    value: '<a class="btn-filled btn-filled-quaternary" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                },
                {
                    text: 'Orange ( Background )',
                    tooltip: 'Hover: Background White',
                    value: '<a class="btn-filled" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                },
                {
                    text: 'Orange ( Background Border )',
                    tooltip: 'Hover: Border Orange',
                    value: '<a class="btn-filled btn-filled--whitebg" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                },
                {
                    text: 'Green ( Background Border )',
                    tooltip: 'Hover: Border Green',
                    value: '<a class="btn-filled btn-filled-secondary" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                },
                {
                    text: 'Blue ( Background Border )',
                    tooltip: 'Hover: Border Blue',
                    value: '<a class="btn-filled btn-filled-tertiary btn-filled--whitebg" href="#">Change Text Here!<span class="btn-arrow"><img class="btn-icon--white" src="https://applegaterecovery.webpagefxstage.com/wp-content/uploads/2021/08/arrow-right.svg" alt="Arrow Right"></span></a>',
                    onclick: function(){
                        editor.insertContent(this.value());
                    }
                }
            ]
        });
    });
});
