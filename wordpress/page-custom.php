<?php
/*
 *
 * Template Name: Custom Template
 *
 * Custom template used in pages such as Leadership Team page
 *
 */
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<main class="custom">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</main>

<?php get_footer();
