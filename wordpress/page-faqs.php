<?php
/*
 *
 * Template Name: Archive FAQs Template
 *
 * Location template for Location Page Homepage
 *
 */
?>
<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<?php $template_url = get_template_directory_uri(); ?>

<?php $fxa_counter = 0; ?>
<!-- The Accordion -->
<section class="fxa-accordion js-accordion section-margins">
    <div class="container">
        <!-- FAQ Filter -->
        <div class="faqs-nav">
            <p class="faqs-nav__title">Filter By:</p>
            <div class="faqs-btn-wrapper">
                <button class="faqs__btn js-filter" data-filter="general-faqs">
                    <span class="faqs__btn-circle active"></span>
                    <p class="faqs__btn-title">General</p>
                </button>
                <button class="faqs__btn js-filter" data-filter="family-faqs">
                    <span class="faqs__btn-circle"></span>
                    <p class="faqs__btn-title">Family</p>
                </button>
                <button class="faqs__btn js-filter" data-filter="all">
                    <span class="faqs__btn-circle"></span>
                    <p class="faqs__btn-title">Show All</p>
                </button>
            </div>
        </div>
        <!-- Panels -->
        <div class="fxa-accordion__panels"></div>
    </div>
</section>

<?php get_footer(); ?>
