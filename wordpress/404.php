<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<div class="container page-404">
    <section class="imgbtns-404">
        <h2>Explore one of these pages instead:</h2>
        <div class="row">
            <?php if( have_rows('links', 'option') ): ?>
                <?php while( have_rows('links', 'option') ): the_row(); ?>
                    <?php
                    $image = get_sub_field('image', 'option');

                    $link = get_sub_field('link', 'option');
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <div class="col-xxs-12 col-sm-6 col-md-3">
                        <a class="imgbtns-box" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>">
                            <?php echo wp_get_attachment_image( $image, '', '', ['class' => 'imgbtns-image'] ); ?>
                            <div class="imgbtns-content">
                                <h5><?php echo $link_title; ?></h5>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>

    <section class="links-404">
        <div class="row">
            <div class="col-xxs-12 col-md-6">
                <div class="search-404">
                    <h2>Or, try searching our site:</h2>
                    <?php get_search_form(); ?>
                </div>
            </div>
            <div class="col-xxs-12 col-md-6">
                <div class="contact-404">
                    <h2>Still can't find what you're looking for?</h2>
                    <a href="https://applegaterecovery.webpagefxstage.com/contact-us/" class="btn btn-quaternary">Contact Us Today!</a>
                </div>
            </div>
        </div>
    </section>

</div>

<?php get_footer();
