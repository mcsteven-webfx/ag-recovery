<?php get_header(); ?>

<?php get_template_part('partials/masthead'); ?>

<?php $template_url = get_template_directory_uri(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <main class="blogs">
        <div class="container">
            <div class="blogs-wrapper">
                <div class="blogs-main">
                    <div class="wysiwyg wysiwyg-component">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-xxs-12">
                        <?php get_template_part( 'partials/social-share' ); ?>
                    </div>
                </div>
                <!-- Archives Page Side -->
                <aside class="blogs-side">
                    <!-- Search Bar -->
                    <div class="search-bar blogs-search-bar">
                        <form class="search-bar-wrapper">
                            <img class="search-bar__icon" src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
                            <input class="search-bar__search" type="text" name="search" placeholder="Search Blog">
                            <button type="submit" class="search-bar__link js-search bg-color--blue" href="./">
                                <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                            </button>
                        </form>
                        <label class="search__error">Search field is empty!</label>
                    </div>
                    <!-- Widget -->
                    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
                </aside>
            </div>
        </div>
    </main>
<?php endwhile; endif; ?>

<?php get_footer();
