<?php
/*
 *
 * Template Name: Contact Template
 *
 * Contact template used for Contact Page Only
 *
 */
?>

<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<main class="contact">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
    <?php //require_once $theme_path . '/partials/jot-form-contact-us.php'; ?>
</main>

<?php get_footer();
