<?php
/*
 *
 * PHP Ajax functionality for Forms and Guides Page
 *
 */


/*
 * Filters
 */
add_action('wp_ajax_filter_guides', 'filter_guides');
add_action('wp_ajax_nopriv_filter_guides', 'filter_guides');
function filter_guides() {
    $slug = $_POST['category'];
    $title = $_POST['title'];
    $page = $_POST['page'];
    // Sanitize the received page
    $cur_page = $page;
    $page -= 1;
    // Set the number of results to display
    $per_page = 5;
    $previous_btn = true;
    $next_btn = true;
    $start = $page * $per_page;

    $count = '';
    $response = '';
    $item = '';

    if( ( strcmp($slug,'guides') == 0 && empty($title) ) || ( strcmp($slug,'forms') == 0 && empty($title) ) ):
        $args = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged,
            'offset' => $start,
            'tax_query' => array(
                array(
                    'taxonomy' => 'pdf',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );

        $count = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'pdf',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );

    elseif( ( strcmp($slug,'guides') == 0 && !empty($title) ) || ( strcmp($slug,'forms') == 0 && !empty($title) ) ):
        $args = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged,
            's' => $title,
            'offset' => $start,
            'tax_query' => array(
                array(
                    'taxonomy' => 'pdf',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );

        $count = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            's' => $title,
            'tax_query' => array(
                array(
                    'taxonomy' => 'pdf',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );

    elseif( strcmp($slug,'all') == 0 && !empty($title) ):
        $args = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged,
            's' => $title,
            'offset' => $start,
        );

        $count = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            's' => $title,
        );

    else:
        $args = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'paged' => $paged,
            'offset' => $start,
        );

        $count = array(
            'post_type' => 'form-guides',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );

    endif;

    $post = new WP_Query( $args );
    $post_count = new WP_Query( $count );

    if($post->have_posts()):
        while($post->have_posts()): $post->the_post();

            $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            $image_alt = get_post_meta(get_post_thumbnail_id($post->ID) , '_wp_attachment_image_alt', true);
            $title = get_the_title();
            $description = get_field('descriptions', get_the_ID());
            $template_url = get_template_directory_uri();

            $item .= '<li class="guides-item animate__animated animate__fadeInUp">';
            $item .= '<img class="guides__img visible-lg visible-md hidden-sm-down" src="'.$image_url.'" alt="'.$image_alt.'">';
            $item .= '<div class="guides-item-wrapper">';
            $item .= '<div class="guides-content">';
            $item .= '<h3>'.$title.'</h3>';
            $item .= '<p>'.$description.'</p>';
            $item .= '</div>';
            $item .= '<a class="guides__link" href="">';
            $item .= '<span class="guides__link-circle">';
            $item .= '<img src="'.$template_url.'/assets/icons/guides-icon.svg" alt="">';
            $item .= '</span>';
            $item .= 'Download</a></div></li>';

        endwhile;

        $content = '<ul class="guides-list">'.$item.'</ul>';
    else:
        if( !empty($title) ):
            $content .= '<ul class="guides-list"><h3 class="animate__animated animate__fadeInUp">No Document Title: '.$title.'</h3></ul>';
        else:
            $content .= '<ul class="guides-list"><h3 class="animate__animated animate__fadeInUp">No Documents</h3></ul>';
        endif;
    endif;

    // This is where the magic happens
    $no_of_paginations = ceil($post_count->found_posts / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3)
            $end_loop = $cur_page + 3;
        else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7)
            $end_loop = 7;
        else
            $end_loop = $no_of_paginations;
    }

    // Pagination Buttons logic
    $pag_container .= '<div class="pagination guides-pagination"><div class="guides-pagination-wrapper">';

    if ($previous_btn && $cur_page > 1) {
        $pre = $cur_page - 1;
        $pag_container .= '<a class="pag-prev js-link" data-pagenum="'.$pre.'"><img src="'.$template_url.'/assets/icons/guides-next.svg" alt=""></a>';
    }

    $pag_container .= '<ul class="pag-list">';

    if( $end_loop != 1){
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $pag_container .= '<li class="pag-item"><a class="pag__link js-link active" data-pagenum="'.$i.'">'.$i.'</a></li>';
            else
                $pag_container .= '<li class="pag-item"><a class="pag__link js-link" data-pagenum="'.$i.'">'.$i.'</a></li>';
        }
    }

    $pag_container .='</ul>';

    if ($next_btn && $cur_page < $no_of_paginations) {
        $nex = $cur_page + 1;
        $pag_container .= '<a class="pag-next js-link active" data-pagenum="'.$nex.'"><img src="'.$template_url.'/assets/icons/guides-next.svg" alt=""></a>';
    }

    $pag_container .='</div></div>';

    $response .= $content;
    $response .= $pag_container;

    echo $response;
    exit;
}
