<?php
/*
 *
 * PHP Ajax functionality for Blog Archive Pages
 *
 */

 /*
  * Filters
  */
 add_action('wp_ajax_filter_faqs', 'filter_faqs');
 add_action('wp_ajax_nopriv_filter_faqs', 'filter_faqs');
 function filter_faqs() {
     $slug = $_POST['filter'];
     $fxa_counter = 0;

     if( strcmp($slug, 'all') == 0 ):
        $args = array(
            'post_type' => 'faqs',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
     else:
        $args = array(
            'post_type' => 'faqs',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'faqs-cat',
                    'field' => 'slug',
                    'terms' => $slug,
                )
            )
        );
     endif;

     $post = new WP_Query( $args );

     if($post->have_posts()):
         while($post->have_posts()): $post->the_post();
             $item .= '<article class="fxa-accordion__panel js-accordion-item animate__animated animate__fadeInUp" data-accordion-id="'.$fxa_counter.'" >';
             $item .= '<button class="fxa-accordion__panel__toggle js-accordion-headline js-fxa-accordion-button" type="button" data-accordion-id="'.$fxa_counter.'">'.get_the_title().'</button>';
             $item .= '<div class="fxa-accordion__panel__content">'.get_the_content().'</div>';
             $item .= '</article>';

             $fxa_counter++;
         endwhile;

         $content = $item;
     else:
         $content .= '<h3 class="animate__animated animate__fadeInUp">No FAQS for '.$slug.'</h3>';
     endif;

     $response .= $content;

     echo $response;
     exit;
 }
