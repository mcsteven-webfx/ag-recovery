<?php
/* Table of Contents
 *
 * Admin Whitelabel
 * TinyMCE Options
 * Block Editor Options
 * Page Excerpts
 */

/** ----- ADMIN WHITELABEL ----- **/

/**
 * Output style to change logo on login
 *
 * @return void
 */
add_action( 'login_head', 'fx_login_logo' );
function fx_login_logo() {

    // check for logo set in WP admin
    $image_id = fx_get_client_logo_image_id();

    if( empty( $image_id ) ) {
        return;
    }

    $image_data = wp_get_attachment_image_src( $image_id, 'small' );

    ?>

    <style type="text/css">
        h1 a {
            width: 250px !important;
            height: 55px !important;
            margin-bottom: 0 !important;
            padding-bottom: 0 !important;
            background-image:url('<?php echo esc_url( $image_data[0] ); ?>') !important;
            background-size: 250px !important;
        }

        .login form { margin-top: 25px !important; }

        #nav {
            float: right !important;
            width: 50%;
            padding: 0 !important;
            text-align: right !important;
        }

        #backtoblog {
            float: left !important;
            width: 50%;
            padding: 0 !important;
            margin-top: 24px;
        }
    </style>

    <?php
}


/**
 * Removes Items from the sidebar that aren't needed
 *
 * @return void
 */
add_action( 'admin_menu', 'fx_remove_admin_menu_items' );
function fx_remove_admin_menu_items() {
    global $menu;

    // array of item names to remove
    $remove_menu_items = array(
        __( 'Comments' ),
    );

    end( $menu );
    while ( prev( $menu ) ) {
        $item = explode( ' ', $menu[ key( $menu ) ][0] );
        if ( in_array( null !== $item[0] ? $item[0] : '', $remove_menu_items, true ) ) {
            unset( $menu[ key( $menu ) ] );
        }
    }
}


/**
 * Removes nodes from admin bar to make for white labeled
 *
 * @param  class $wp_toolbar the WordPress toolbar instance.
 * @return class             returns the modified toolbar
 */
add_action( 'admin_bar_menu', 'fx_remove_admin_bar_menus', 999 );
function fx_remove_admin_bar_menus( $wp_toolbar ) {
    $wp_toolbar->remove_node( 'wp-logo' );
    return $wp_toolbar;
}


/**
 * Remove the defualt dashboard widgets for orgs
 *
 * @return void
 */
add_action( 'wp_dashboard_setup', 'fx_remove_dashboard_widgets', 0 );
function fx_remove_dashboard_widgets() {
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
}


/**
 * Remove the WordPress text at the bottom of the admin
 *
 * @param  string $text current footer text.
 * @return string the changed footer text
 */
add_filter( 'update_footer', 'fx_remove_footer_text', 999 );
add_filter( 'admin_footer_text', 'fx_remove_footer_text' );
function fx_remove_footer_text() {
    return '';
}


/**
 * Change logo URL on WP login page to point to site's homepage
 *
 * @return string 	Homepage URL
 */
add_filter( 'login_headerurl', function() {
	return get_home_url();
});

/** ----- TINYMCE OPTIONS ----- **/

/**
 * Add "Styles" drop-down
 *
 * @param  array $buttons current buttons to be setup.
 * @return array
 */
add_filter( 'mce_buttons_1', 'fx_mce_editor_buttons' );
function fx_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

// Add Custom Dropdowns on TinyMCE
add_filter( 'mce_buttons_2', 'fx_custom_tinymce_button');
function fx_custom_tinymce_button($buttons) {
    $buttons[] = 'editor_buttons_dropdown';
    return $buttons;
}

// Add Custom TinyMCE Scripts
add_filter('mce_external_plugins', 'fx_tinymce_plugins');
function fx_tinymce_plugins() {
    $theme_path = get_template_directory_uri();
    $plugins_array["mce_editor_js"] = $theme_path . '/assets/js/editor.js';
    return $plugins_array;
}

/**
 * Add styles/classes to the "Styles" drop-down
 *
 * @param  array $settings Settings array for TinyMCE.
 * @return array
 */
add_filter( 'tiny_mce_before_init', 'fx_mce_before_init' );
function fx_mce_before_init( $settings ) {
    $style_formats = array(
        array(
            'title' => 'Headings',
            'items' => array(
                array(
                    'title' => 'Heading H1',
                    'block' => 'h1',
                ),
                array(
                    'title' => 'Heading H2',
                    'block' => 'h2',
                ),
                array(
                    'title' => 'Heading H3',
                    'block' => 'h3',
                ),
                array(
                    'title' => 'Heading H4',
                    'block' => 'h4',
                ),
                array(
                    'title' => 'Heading H4 Leaf',
                    'block' => 'h4',
                    'classes' => 'leaf',
                ),
                array(
                    'title' => 'Heading H4 White Leaf',
                    'block' => 'h4',
                    'classes' => 'leaf leaf--white',
                ),
                array(
                    'title' => 'Heading H4 Center',
                    'block' => 'div',
                    'classes' => 'leaf--center',
                    'wrapper' => true,
                ),
                array(
                    'title' => 'Heading H5',
                    'block' => 'h5',
                ),
                array(
                    'title' => 'Heading H6',
                    'block' => 'h6',
                ),
            )
        ),
        array(
            'title' => 'Blocks',
            'items' => array(
                array(
                    'title' => 'Paragraph',
                    'block' => 'p',
                ),
                array(
                    'title' => 'Multiple Button Wrapper',
                    'block' => 'div',
                    'classes' => 'btn-wrapper',
                    'wrapper' => true,
                ),
                array(
                    'title' => 'Button Center',
                    'block' => 'div',
                    'classes' => 'btn-center',
                    'wrapper' => true,
                ),
            )
        ),
        array(
            'title' => 'Selectors',
            'items' => array(
                array(
                    'title' => 'Contact',
                    'selector' => 'a',
                    'classes' => 'contact',
                ),
            )
        ),
        array(
            'title' => 'Homepage Slider',
            'items' => array(
                array(
                    'title' => 'Heading',
                    'block' => 'h1',
                    'classes' => 'home-slide-content-heading'
                ),
                array(
                    'title' => 'Underline Wrapper',
                    'block' => 'p',
                    'classes' => 'home-slide-content-wrapper'
                ),
                array(
                    'title' => 'Underline',
                    'inline' => 'span',
                    'classes' => 'home-slide-content-span'
                )
            )
        ),
    );

    $settings['style_formats'] = wp_json_encode( $style_formats );

    return $settings;
}


/**
 *  Adds "Theme Settings" option page
 *
 *  @return  void
 */
add_action( 'init', 'fx_admin_add_options_page' );
function fx_admin_add_options_page() {
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(
            [
                'page_title'    => 'Theme General Settings',
                'menu_title'    => 'Theme Settings',
                'menu_slug'     => 'theme-general-settings',
                'capability'    => 'edit_posts',
                'redirect'      => false
            ]
        );
    }
}

/** ----- BLOCK EDITOR OPTIONS ----- **/

/**
 * Remove core block patterns to prevent user confusion
 */
add_action( 'after_setup_theme', 'fx_remove_core_block_patterns' );
function fx_remove_core_block_patterns() {
    remove_theme_support( 'core-block-patterns' );
}

/**
 * Unregister the "Classic Block" to prevent admin confusion
 */
add_action( 'init', 'fx_unregister_classic_block', 11 );
function fx_unregister_classic_block() {
    unregister_block_type( 'core/freeform' );
}

/**
 * Unregisters the CF7 block, since does not allow you to include
 * an html_id for MCFX tracking. Use the FX CF7 block instead.
 *
 * TODO remove from build template in phase II (for builds, delete this comment)
 */
add_action( 'init', 'fx_unregister_cf7_block', 11 );
function fx_unregister_cf7_block() {
    if( WP_Block_Type_Registry::get_instance()->is_registered( 'contact-form-7/contact-form-selector' ) ) {
        unregister_block_type( 'contact-form-7/contact-form-selector' );
    }
}

/**
 * Restrict the blocks that can be used on the homepage: this should include the top-level acf/homepage-block.
 * Inner-blocks declared in homepage-block.php will automatically be included
 *
 */
add_filter( 'allowed_block_types_all', 'fx_restrict_homepage_blocks', 10, 2 );
function fx_restrict_homepage_blocks( $allowed_blocks, WP_Block_Editor_Context $block_editor_context ) {
    $post = $block_editor_context->post;

    if( isset( $post->ID ) && $post->ID === absint( get_option( 'page_on_front' ) ) ) {
        $allowed_blocks = [ 'acf/homepage-block' ];

    } else {
        // @todo — add blocks that should be only on homepage. Block name should be acf/{name}
        $disallowed_blocks = [
            'acf/homepage-block',
            'acf/homepage-slider',
            'acf/homepage-about',
            'acf/homepage-treatment',
            'acf/homepage-steps',
            'acf/homepage-location',
            'acf/homepage-insurance',
            'acf/homepage-resource',
        ];

        if( is_bool( $allowed_blocks ) ) {
            $block_types = WP_Block_Type_Registry::get_instance()->get_all_registered();
            $allowed_blocks = array_keys( $block_types );
        }

        foreach( $disallowed_blocks as $block_to_unset ) {
            $key = array_search( $block_to_unset, $allowed_blocks );

            if( false !== $key ) {
                unset( $allowed_blocks[ $key ] );
            }
        }
    }

    if( is_array( $allowed_blocks ) ) {
        $allowed_blocks = array_values( $allowed_blocks );
    }

    return $allowed_blocks;
}

/**
 * Add Another Option Page for Company Manager Plugin
 */

if( function_exists('acf_add_options_page') ) {
    acf_add_options_sub_page(array(
        'page_title'     => 'State Links',
        'menu_title'    => 'State Links',
        'parent_slug'    => 'edit.php?post_type=location',
    ));
}
