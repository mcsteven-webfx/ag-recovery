<?php get_header(); ?>

<?php get_template_part( 'partials/masthead' ); ?>

<?php $template_url = get_template_directory_uri(); ?>

<main class="blogs">
    <div class="container">
        <?php $category = get_term( get_query_var('cat'), 'category' );  ?>
        <div class="blogs-wrapper" data-cat="<?php echo $category->slug; ?>">
            <!-- Archives Page Main -->
            <div class="blogs-main"></div>
            <!-- Archives Page Side -->
            <aside class="blogs-side">
                <!-- Search Bar -->
                <div class="search-bar blogs-search-bar">
                    <form class="search-bar-wrapper">
                        <img class="search-bar__icon" src="<?php echo $template_url; ?>/assets/icons/search-icon.svg" alt="Search">
                        <input class="search-bar__search" type="text" name="search" placeholder="Search Blog">
                        <button type="submit" class="search-bar__link js-search bg-color--blue" href="./">
                            <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                        </button>
                    </form>
                    <label class="search__error">Search field is empty!</label>
                </div>
                <!-- Widget -->
                <?php dynamic_sidebar( 'blog-sidebar' ); ?>
            </aside>
        </div>
    </div>
</main>


<?php get_footer(); ?>
