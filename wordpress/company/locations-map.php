
<?php
    $user_zipcode = null;
    if( ! empty( $_POST[ 'zipcode' ] ) ) {
        $user_zipcode = filter_var( $_POST[ 'zipcode' ], FILTER_SANITIZE_STRING );
    }

    $template_url = get_template_directory_uri();
?>
<?php if ( ! is_singular( 'location' ) ): ?>
<!-- Zipcode Search -->
    <div class="wpcm-zip" id="wpcm_zip"><?php // do not change "wpcm-zip" class or "wpcm_zip" ID ?>
        <form class="location-form wpcm-zip__form" method="POST"><?php // do not change "wpcm-zip__form" class ?>
            <h4 class="location-form__title">START YOUR SEARCH</h4>

            <div class="location-form-wrapper">
                <input type="number" name="zipcode" class="wpcm-zip__zipcode" value="<?php echo $user_zipcode; ?>" placeholder="Enter Your Zip Code"><?php // do not rename input or change "wpcm-zip__zipcode" class ?>
                <select name="distance" class="wpcm-zip__distance">
                    <option value="25">Select Radius</option> <?php //default to 25 ?>
                    <option value="5">5 Miles</option>
                    <option value="10">10 Miles</option>
                    <option value="15">15 Miles</option>
                    <option value="20">20 Miles</option>
                    <option value="25">25 Miles</option>
                    <option value="30">30 Miles</option>
                    <option value="50">50 Miles</option>
                    <option value="100">100 Miles</option>
                    <option value="200">200 Miles</option>
                    <option value="500">500 Miles</option>
                </select>
                <input type="hidden" name="imahuman" id="imahuman" value=""><?php // do not rename input or change "imahuman" id ?>
                <input type="hidden" name="state" id="state" value="<?php echo $state; ?>"><?php // do not rename input or change "state" id ?>
                <button type="submit" name="submit" class="wpcm-zip__submit location-form__link bg-color--blue">
                    <img class="btn-icon--white" src="<?php echo $template_url; ?>/assets/icons/arrow-right.svg" alt="Arrow Right">
                </button>
            </div>
        </form>
    </div>

    <div class="location-key">
        <h5 class="location__title">Map Key:</h5>
        <ul class="location-list">
            <li class="location-item">
                <span class="location__icon icon-first"></span>
                <p class="location__text text-color--white">AppleGate Recovery</p>
            </li>
            <li class="location-item">
                <span class="location__icon icon-second"></span>
                <p class="location__text text-color--white">Middlesex Recovery</p>
            </li>
        </ul>
    </div>

<!-- /Zipcode Search -->
<?php endif; ?>

<!-- Map -->
<div class="location-map" id="locations_map" style="width:100% !important;height:564px !important;"></div><?php // do not change "locations_map" ID ?>
<!-- /Map -->

<script type="text/template" id="markerTemplate"><?php // do not change "markerTemplate" ID ?>
    <div class="map-marker">
        <h4><%= title %></h4>
        <p><%= hours %></p><br>
        <a class="map__phone" href="tel:<%= phone %>"><span><%= phone%></span></a>
        <a class="map__direction" target="_blank" href="<%= directions %>">Get Directions</a>
        <?php if ( ! is_singular( 'location' ) ): // Only show this button on main map page ?>
            <a class="map__single btn btn-quaternary" href="<%= url %>">View Location Details</a>
        <?php endif; ?>
    </div>
</script>
