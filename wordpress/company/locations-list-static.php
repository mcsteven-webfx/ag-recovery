<!-- Static Locations List Template -->

<?php $template_url = get_template_directory_uri(); ?>

<section class="location-cards">
    <!-- Outline -->
    <img class="location-cards__outline" src="<?php echo $template_url; ?>/assets/img/inner-outline.svg" alt="">
    <!-- Wrapper -->
    <div class="location-cards-wrapper">
        <div class="container">
            <?php
            // Get all states
            $values = [];
            $category = [];

            foreach ( $locations as $location ):
                array_push( $values, get_field('state', get_the_ID()) );
            endforeach;

            // Sort Values
            sort( $values );
            $category = array_unique($values);

            foreach ($category as $cat):

            ?>
                <div class="location-state wow animate__animated animate__fadeInUp" data-wow-offset="250">
                    <h2 class="location-state__title"><?php echo $cat; ?></h2>
                    <div class="location-state-list">

                        <?php foreach ( $locations as $location ):

                            $state = $location->state;
                            $address = $location->address;

                            if( strcmp($state, $cat) == 0 ):
                        ?>
                                <div class="location-state-box">
                                    <h2 class="location-state__city">  <?php echo $location->title; ?></h2>
                                    <h4 class="location-state__state"><?php echo $cat; ?></h4>
                                    <p class="location-state__address"><?php echo $address; ?></p>
                                    <?php
                                    $link = $location->phone;
                                    ?>
                                    <a class="location-state__contact" href="tel:<?php echo $link; ?>">
                                        <img src="<?php echo $template_url; ?>/assets/icons/location-phone.svg" alt="Location Phone">
                                        <?php echo $link; ?>
                                    </a>
                                    <a class="btn btn-quaternary location-state__link" href="<?php echo $location->url; ?>">More Info</a>
                                </div>         
                            <?php endif; ?>            

                    <?php endforeach; ?>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
