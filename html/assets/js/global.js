/* ---------------------------------------------------------------------
	Global Js
	Target Browsers: All

	HEADS UP! This script is for general functionality found on ALL pages and not tied to specific components, blocks, or
	plugins.

	If you need to add JS for a specific block or component, create a script file in js/components or js/blocks and
	add your JS there. (Don't forget to enqueue it!)
------------------------------------------------------------------------ */

var FX = ( function( FX, $ ) {

	/**
	 * Doc Ready
	 *
	 * Use a separate $(function() {}) block for each function call
	 */
	$( () => {
		FX.General.init(); // For super general or super short scripts
	})

    $( () => {
        //FX.ExternalLinks.init(); // Enable by default
	})

	$( () => {
		FX.MobileMenu.init();
	})

    $( () => {
		FX.CustomSelect.init();
	})

	$( () => {
		FX.HeaderSticky.init();
	})

	$( () => {
		FX.HeaderSearch.init();
	})

	$( () => {
		FX.MenuSearchNotUsing.init();
	})


	$( () => {
		FX.WowJs.init();
	})

	$(window).on( 'load', () => {
		FX.BackToTop.init()
	})



	/**
	 * Example Code Block - This should be removed
	 * @type {Object}
	 */
	FX.MenuSearchNotUsing = {
		init() {
			$('main').click(function() {
				$('.header-bar').removeClass('active');
				$('.js-mobile').removeClass('active');
				$('.js-mobile-dropdown').removeClass('active');
			});
		},
	};

	/**
	 * WowJs
	 * @type {Object}
	 */
	FX.WowJs = {
		init() {
			new WOW().init();
		},
	};


	/**
	 * Header Sticky
	 * @type {Object}
	 */
	FX.HeaderSticky = {
		init() {
			var sticky = $('.js-sticky');
			var stickyOffset = sticky.offset().top;
			var outerHeight = sticky.outerHeight();
			var bottomOffset = stickyOffset + outerHeight;

			$(window).scroll(function(){
				scroll = $(window).scrollTop();

				if (scroll >= bottomOffset) {
					$('.header-sticky').addClass('sticky');
				}
				else {
					$('.header-sticky').removeClass('sticky');
				}
			});
		},
	};

	/**
	 * Example Code Block - This should be removed
	 * @type {Object}
	 */
	FX.HeaderSearch = {
		init() {
			$searchToggle = $('.js-search');
			$searchToggle.click(function () {
				$('.header-bar').toggleClass('active');
				$('.js-mobile').removeClass('active');
				$('.js-mobile-dropdown').removeClass('active');
			});

		},
	};

	/**
	 * Custom Select
	 * @type {Object}
	 */
	FX.CustomSelect = {
		init() {
			// Iterate over each select element
			$('select').each(function () {

			    // Cache the number of options
			    var $this = $(this),
			        numberOfOptions = $(this).children('option').length;

			    // Hides the select element
			    $this.addClass('invisible');

			    // Wrap the select element in a div
			    $this.wrap('<div class="custom-select"></div>');

			    // Insert a styled div to sit over the top of the hidden select element
			    $this.after('<div class="custom__selected"></div>');

			    // Cache the styled div
			    var $styledSelect = $this.next('div.custom__selected');

			    // Show the first select option in the styled div
			    $styledSelect.text($this.children('option').eq(0).text());

			    // Insert an unordered list after the styled div and also cache the list
			    var $list = $('<ul />', {
			        'class': 'options'
			    }).insertAfter($styledSelect);

			    // Insert a list item into the unordered list for each select option
			    for (var i = 0; i < numberOfOptions; i++) {
			        $('<li />', {
			            text: $this.children('option').eq(i).text(),
			            rel: $this.children('option').eq(i).val()
			        }).appendTo($list);
			    }

			    // Cache the list items
			    var $listItems = $list.children('li');

			    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
			    $styledSelect.click(function (e) {
			        e.stopPropagation();
			        $('div.custom__selected.active').each(function () {
			            $(this).removeClass('active').next('ul.options').hide();
			        });
			        $(this).toggleClass('active').next('ul.options').toggle();
			    });

			    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
			    // Updates the select element to have the value of the equivalent option
			    $listItems.click(function (e) {
			        e.stopPropagation();
			        $styledSelect.text($(this).text()).removeClass('active');
			        $this.val($(this).attr('rel'));
			        $list.hide();
			        /* alert($this.val()); Uncomment this for demonstration! */
			    });

			    // Hides the unordered list when clicking outside of it
			    $(document).click(function () {
			        $styledSelect.removeClass('active');
			        $list.hide();
			    });
			});
		},
	};

	/**
	 * Display scroll-to-top after a certain amount of pixels
	 * @type {Object}
	 */
	FX.BackToTop = {
		$btn: null,

		init() {
			this.$btn = $('.back-to-top');

			if( this.$btn.length ) {
				this.bind();
			}
		},

		bind() {
			$(window).on( 'scroll load', this.maybeShowButton.bind( this ) );
			this.$btn.on( 'click', this.scrollToTop );
		},

		maybeShowButton() {
			if( $( window ).scrollTop() > 100 ) { // TODO: Update "100" for how far down page to show button
				this.$btn.removeClass( 'hide' );
			} else {
				this.$btn.addClass( 'hide' );
			}
		},

		scrollToTop() {
			$(window).scrollTop( 0 );
		}
	};

	/**
	 * General functionality — ideal for one-liners or super-duper short code blocks
	 */
	FX.General = {
		init() {
			this.bind();
		},

		bind() {

			// Makes all PDF to open in new tabs
			$('a[href*=".pdf"]').each( e => {
				$(this).attr('target', '_blank');
			});

			// FitVids - responsive videos
			//$('body').fitVids();

			// Input on focus remove placeholder
			$('input,textarea').focus( () => {
				$(this).removeAttr('placeholder');
			});

			// nav search toggle
			$('.js-search-toggle').on('click', () => {
				$('.desktop-menu__phone, .js-search-toggle, .desktop-menu__search').toggleClass('js-search-active');
                $('.desktop-menu__search input[name="s"]').focus();
			});

			/* ubermenu hack to force-show a Ubermenu submenu. Delete prior to launch */
			// setInterval(function() {
				// 	$('#menu-item-306').addClass('ubermenu-active');
				// }, 1000 );

				// TODO: Add additional small scripts below
		}
	};

	/**
	 * Mobile menu script for opening/closing menu and sub menus
	 * @type {Object}
	 */
	FX.MobileMenu = {
		init() {
			$submenuToggle = $('.sub-menu-toggle');
			$submenuToggle.click(function () {
				$parent = $(this).closest( 'li' ),
				$wrap = $parent.find( '> .sub-menu' );

				$(this).toggleClass('active');
				$wrap.toggleClass('active');
			});

			var $mobileToggle = $('.js-mobile');
			var $mobileNavToggle = $('.js-mobile-dropdown');

			$mobileToggle.click(function () {
				$mobileToggle.toggleClass('active');
				$mobileNavToggle.toggleClass('active');
				$('.header-bar').removeClass('active');
			});
		}
	};

	/**
	 * Force External Links to open in new window.
	 * @type {Object}
	 */
	FX.ExternalLinks = {
		init() {
			var siteUrlBase = FX.siteurl.replace( /^https?:\/\/((w){3})?/, '' );

			$( 'a[href*="//"]:not([href*="'+siteUrlBase+'"])' )
				.not( '.ignore-external' ) // ignore class for excluding
				.addClass( 'external' )
				.attr( 'target', '_blank' )
				.attr( 'rel', 'noopener' );
		}
	};

	/**
	 * Affix
	 * Fixes sticky items on scroll
	 * @type {Object}
	 */
	FX.Affix = {

		$body: 			null,
		$header: 		null,
		headerHeight: 	null,
		scrollFrame: 	null,
		resizeFrame: 	null,


		init() {
			this.$body 			= $(document.body);
			this.$header 		= $('#page-header');
			this.headerHeight 	= this.$header.outerHeight( true );

			this.bind();
        },


        bind(e) {
			$(window).on( 'scroll', this.handleScroll.bind( this ) );
			$(window).on( 'resize', this.handleResize.bind( this ) );
		},


		handleScroll( e ) {
			var self = this;

			// avoid constantly running intensive function(s) on scroll
			if( null !== self.scrollFrame ) {
				cancelAnimationFrame( self.scrollFrame )
			}

			self.scrollFrame = requestAnimationFrame( self.maybeAffixHeader.bind( self ) )
		},


		handleResize( e ) {
			var self = this;

			// avoid constantly running intensive function(s) on resize
			if( null !== self.resizeFrame ) {
				cancelAnimationFrame( self.resizeFrame )
			}

			self.resizeFrame = requestAnimationFrame( () => {
				self.headerHeight = self.$header.outerHeight( true );
			})
		},


		maybeAffixHeader() {
			var self = this;

			if( 200 < $(window).scrollTop() ) {
				self.$body.css( 'padding-top', self.headerHeight );
				self.$header.addClass('js-scrolled');
			} else {
				self.$body.css( 'padding-top', 0 );
				self.$header.removeClass('js-scrolled');
			}
		}
	};

	/**
	 * FX.SmoothAnchors
	 * Smoothly Scroll to Anchor ID
	 * @type {Object}
	 */
	FX.SmoothAnchors = {
		hash: null,

		init() {
			this.hash = window.location.hash;

			if( '' !== this.hash ) {
				this.scrollToSmooth( this.hash );
			}

			this.bind();
		},

		bind() {
			$( 'a[href^="#"]' ).on( 'click', $.proxy( this.onClick, this ) );
		},

		onClick( e ) {
			e.preventDefault();

			var target = $( e.currentTarget ).attr( 'href' );

			this.scrollToSmooth( target );
		},

		scrollToSmooth( target ) {
			var $target = $( target ),
				headerHeight = 0 // TODO: if using sticky header change to $('#page-header').outerHeight(true)

			$target = ( $target.length ) ? $target : $( this.hash );

			if ( $target.length ) {
				var targetOffset = $target.offset().top - headerHeight;

				$( 'html, body' ).animate({
					scrollTop: targetOffset
				}, 600 );

				return false;
			}
		}
	};

	return FX;

} ( FX || {}, jQuery ) );
